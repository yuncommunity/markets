package routers

import (
	"markets/api/controllers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "X-Requested-With"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))

	ns := beego.NewNamespace("/接口",
		beego.NSNamespace("/七牛", beego.NSInclude(&controllers.C七牛{})),
		beego.NSNamespace("/产品", beego.NSInclude(&controllers.C产品{})),
		beego.NSNamespace("/分类", beego.NSInclude(&controllers.C分类{})),
		beego.NSNamespace("/单位", beego.NSInclude(&controllers.C单位{})),
		beego.NSNamespace("/地址", beego.NSInclude(&controllers.C地址{})),
		beego.NSNamespace("/用户", beego.NSInclude(&controllers.C用户{})),
		beego.NSNamespace("/订单", beego.NSInclude(&controllers.C订单{})),
		beego.NSNamespace("/邀请码", beego.NSInclude(&controllers.C邀请码{})),
		beego.NSNamespace("/登录记录", beego.NSInclude(&controllers.C登录记录{})),
	)

	beego.AddNamespace(ns)

	beego.Router("/更新", &controllers.MainController{}, "*:A更新")
	beego.Router("/*", &controllers.MainController{})
}
