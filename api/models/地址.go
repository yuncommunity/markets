package models

import "time"

type M地址 struct {
	Id       int `storm:"id,increment"`
	F用户Id    int
	F名称      string
	F性别      string
	F手机      string
	F纬度      float64
	F经度      float64
	F地址      string
	F详细地址    string
	F设置默认的时间 time.Time
	F创建时间    time.Time
}
