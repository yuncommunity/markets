package models

import (
	"time"
)

type M产品 struct {
	Id    int `storm:"id,increment"`
	F名称   string
	F简介   string
	F二维码  string
	F图片   []string
	F缩略图  string
	F价格   float64
	F单位   M单位
	F分类   M分类
	F创建时间 time.Time
	F数量   int
}

func (m M产品) G详情() M产品 {
	if len(m.F图片) > 0 {
		m.F缩略图 = 获取图片地址(m.F图片[0])
		for i, v := range m.F图片 {
			m.F图片[i] = 获取图片地址(v)
		}
	}
	return m
}
