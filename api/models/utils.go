package models

import (
	"strings"

	"github.com/astaxie/beego"
)

func 获取图片地址(图片名称 string) string {
	if 图片名称 == "" {
		return ""
	}
	if strings.HasPrefix(图片名称, "http") {
		return 图片名称
	}
	return "http://" + beego.AppConfig.String("文件地址") + "/" + 图片名称
}
