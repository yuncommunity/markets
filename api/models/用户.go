package models

import "time"

type M用户 struct {
	Id    int    `storm:"id,increment"`
	F昵称   string `storm:"unique"`
	F密码   string
	F邮箱   string
	F手机   string
	F头像   string
	Token string
	F创建时间 time.Time
}

func (m M用户) G信息() M用户 {
	m.F头像 = 获取图片地址(m.F头像)
	m.F密码 = ""
	return m
}
