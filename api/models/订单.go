package models

import "time"

type M订单 struct {
	Id      int `storm:"id,increment"`
	F用户Id   int
	F地址     M地址
	F产品列表   []M产品
	F预计送达时间 string
	F创建时间   time.Time
}
