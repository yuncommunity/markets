package main

import (
	"markets/api/controllers"
	"markets/api/models"
	_ "markets/api/routers"
	"os"
	"time"

	"github.com/asdine/storm"
	"github.com/astaxie/beego"
	"github.com/oldfeel/ofutils"
)

func main() {
	isFirst := false
	if !ofutils.Exist("db") {
		os.MkdirAll("db", os.ModePerm)
	}
	if !ofutils.Exist("db/data.db") {
		isFirst = true
	}

	var err error
	controllers.DB, err = storm.Open("db/data.db")
	if err != nil {
		beego.Error(err)
	}
	if isFirst {
		controllers.DB.Save(&models.M用户{F昵称: "admin", F密码: ofutils.MD5("admin"), F创建时间: time.Now()})
	}

	beego.Run()
}
