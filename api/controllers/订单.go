package controllers

import (
	"markets/api/models"

	"github.com/asdine/storm/q"

	"github.com/astaxie/beego"
)

type C订单 struct {
	BaseController
}

// @router 提交 [get]
func (c *C订单) A提交() {
	if c.UnLogin() {
		return
	}

	var m models.M订单
	err := c.ParseForm(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}

	用户 := c.GetUser()
	m.F用户Id = 用户.Id
	err = DB.Save(&m)
	if err != nil {
		c.Fail(2, err)
		return
	}
	c.Success("")
}

// @router 分页 [get]
func (c *C订单) A分页() {
	if c.UnLogin() {
		return
	}

	用户 := c.GetUser()

	页, _ := c.GetInt("页")
	页长, _ := c.GetInt("页长")
	页 = 页 - 1

	var 列表 []models.M订单
	query := DB.Select(q.Eq("F用户Id", 用户.Id)).Limit(页长).Skip(页长 * 页).Reverse()
	err := query.Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	c.Page(列表, &models.M订单{}, query)
}

// @router 详情 [get]
func (c *C订单) A详情() {
	Id, _ := c.GetInt("Id")
	var m models.M订单
	err := DB.One("Id", Id, &m)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(m)
}
