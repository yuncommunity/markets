package controllers

import (
	"markets/api/models"

	"github.com/astaxie/beego"
)

type C登录记录 struct {
	BaseController
}

// @router /分页 [get]
func (c *C登录记录) A分页() {
	页, _ := c.GetInt("页")
	页长, _ := c.GetInt("页长")
	页 = 页 - 1

	var 列表 []models.M登录记录
	query := DB.Select().Limit(页长).Skip(页长 * 页).Reverse()
	err := query.Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	for i, v := range 列表 {
		DB.One("Id", v.F用户Id, &列表[i].F用户)
	}
	c.Page(列表, &models.M登录记录{}, query)
}
