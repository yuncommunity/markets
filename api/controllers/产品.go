package controllers

import (
	"encoding/json"
	"markets/api/models"
	"time"

	"github.com/asdine/storm/q"
	"github.com/astaxie/beego"
)

type C产品 struct {
	BaseController
}

// @router 保存 [get]
func (c *C产品) A保存() {
	if c.UnLogin() {
		return
	}
	var m models.M产品
	err := c.ParseForm(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}
	if m.Id == 0 {
		m.F创建时间 = time.Now()
	}
	err = DB.Save(&m)
	if err != nil {
		c.Fail(2, err)
		return
	}
	c.Success(m)
}

// @router 删除 [get]
func (c *C产品) A删除() {
	Ids := c.GetString("Ids")
	var ids []int
	err := json.Unmarshal([]byte(Ids), &ids)
	if err != nil {
		c.Fail(1, err)
		return
	}

	for _, v := range ids {
		err := DB.Delete("M产品", v)
		if err != nil {
			beego.Info(v, err)
		}
	}
	c.Success("删除成功")
}

// @router 分页 [get]
func (c *C产品) A分页() {
	页, _ := c.GetInt("页")
	页长, _ := c.GetInt("页长")
	搜索 := c.GetString("搜索")
	页 = 页 - 1

	var 列表 []models.M产品
	query := DB.Select(q.Re("F名称", 搜索)).Limit(页长).Skip(页长 * 页).Reverse()
	err := query.Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	for i, v := range 列表 {
		列表[i] = v.G详情()
	}
	c.Page(列表, &models.M产品{}, query)
}

// @router 详情 [get]
func (c *C产品) A详情() {
	Id, _ := c.GetInt("Id")
	var m models.M产品
	err := DB.One("Id", Id, &m)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(m.G详情())
}
