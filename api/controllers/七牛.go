package controllers

import (
	"github.com/qiniu/api.v7/auth/qbox"
	"github.com/qiniu/api.v7/storage"
)

var (
	AK = "V_NFErwixCiC-PBkfAQhFLm44oW6v7XIDUYLLEfu"
	SK = "W1uIofGmr7kM4yWC9jRDaqtOFuPPpvfp9lnUabBY"
)

type C七牛 struct {
	BaseController
}

// @router /token [get]
func (c *C七牛) Token() {
	mac := qbox.NewMac(AK, SK)

	bucket := "markets"
	putPolicy := storage.PutPolicy{
		Scope: bucket,
	}
	upToken := putPolicy.UploadToken(mac)
	c.Success(upToken)
}
