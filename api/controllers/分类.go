package controllers

import (
	"encoding/json"
	"markets/api/models"
	"time"

	"github.com/asdine/storm/q"

	"github.com/astaxie/beego"
)

type C分类 struct {
	BaseController
}

// @router 列表 [get]
func (c *C分类) A列表() {
	var 列表 []models.M分类
	err := DB.All(&列表)
	if err != nil {
		beego.Info(err)
	}
	c.Success(列表)
}

// @router 详情 [get]
func (c *C分类) A详情() {
	Id, _ := c.GetInt("Id")
	var m models.M分类
	err := DB.One("Id", Id, &m)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(m)
}

// @router 保存 [get]
func (c *C分类) A保存() {
	if c.UnLogin() {
		return
	}

	var m models.M分类
	err := c.ParseForm(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}

	var tmp models.M分类
	err = DB.Select(q.Eq("F名称", m.F名称)).First(&tmp)
	if err == nil && tmp.Id != m.Id {
		c.Fail(2, m.F名称+"已经存在")
		return
	}

	if m.Id == 0 {
		m.F创建时间 = time.Now()
	}

	err = DB.Save(&m)
	if err != nil {
		c.Fail(3, err)
		return
	}
	c.Success(m)
}

// @router 删除 [get]
func (c *C分类) A删除() {
	Ids := c.GetString("Ids")
	var ids []int
	err := json.Unmarshal([]byte(Ids), &ids)
	if err != nil {
		c.Fail(1, err)
		return
	}

	for _, v := range ids {
		err := DB.Delete("M分类", v)
		if err != nil {
			beego.Info(v, err)
		}
	}
	c.Success("删除成功")
}
