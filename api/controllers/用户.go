package controllers

import (
	"encoding/json"
	"markets/api/models"
	"time"

	"github.com/menduo/gobaidumap"
	"github.com/oldfeel/ofutils"

	"github.com/asdine/storm/q"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/utils/captcha"
)

var cpt *captcha.Captcha

func init() {
	store := cache.NewMemoryCache()
	cpt = captcha.NewWithFilter("/验证码图片/", store)
	cpt.StdWidth = 118
	cpt.StdHeight = 40
}

type C用户 struct {
	BaseController
}

// @router 分页 [get]
func (c *C用户) A分页() {
	页, _ := c.GetInt("页")
	页长, _ := c.GetInt("页长")
	搜索 := c.GetString("搜索")
	页 = 页 - 1

	var 列表 []models.M用户
	query := DB.Select(q.Re("F昵称", 搜索)).Limit(页长).Skip(页长 * 页).Reverse()
	err := query.Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	for i, _ := range 列表 {
		列表[i].F密码 = ""
	}
	c.Page(列表, &models.M用户{}, query)
}

// @router 删除 [get]
func (c *C用户) A删除() {
	Ids := c.GetString("Ids")
	var ids []int
	err := json.Unmarshal([]byte(Ids), &ids)
	if err != nil {
		c.Fail(1, err)
		return
	}

	for _, v := range ids {
		err := DB.Delete("M用户", v)
		if err != nil {
			beego.Info(v, err)
		}
	}
	c.Success("删除成功")
}

// @router 保存 [get]
func (c *C用户) A保存() {
	var m models.M用户
	err := c.ParseForm(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}
	if m.Id != 0 {
		var tmp models.M用户
		err = DB.One("Id", m.Id, &tmp)
		if err != nil {
			c.Fail(2, "用户不存在")
			return
		}

		if m.F密码 != "" {
			m.F密码 = ofutils.MD5(m.F密码)
		} else {
			m.F密码 = tmp.F密码
		}
	} else {
		m.F密码 = ofutils.MD5(m.F密码)
		m.F创建时间 = time.Now()
	}

	err = DB.Save(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(m.G信息())
}

// @router 登录 [get]
func (c *C用户) A登录() {
	F昵称 := c.GetString("用户名")
	F密码 := c.GetString("密码")

	var m models.M用户
	err := DB.Select(q.Eq("F昵称", F昵称), q.Eq("F密码", ofutils.MD5(F密码))).First(&m)
	if err != nil {
		c.Fail(1, "账号或密码错误")
		return
	}

	m.Token = ofutils.RandString(32)
	DB.Save(&m)

	go func() {
		ip := c.Ctx.Input.IP()

		IPToAddress, _ := gobaidumap.GetAddressViaIP(ip)
		address := IPToAddress.Content.Address
		登录记录 := models.M登录记录{F创建时间: time.Now(), Ip: ip, F地址: address, F用户Id: m.Id}
		DB.Save(&登录记录)
	}()

	c.Success(m.G信息())
}

// @router /获取用户信息 [get]
func (c *C用户) A获取用户信息() {
	if c.UnLogin() {
		return
	}
	用户 := c.GetUser()

	c.Success(用户.G信息())
}

// @router /注册 [get]
func (c *C用户) A注册() {
	用户名 := c.GetString("用户名")
	密码 := c.GetString("密码")
	邮箱 := c.GetString("邮箱")
	验证码Id := c.GetString("验证码Id")
	验证码 := c.GetString("验证码")
	if !cpt.Verify(验证码Id, 验证码) {
		c.Fail(1, "验证码错误")
		return
	}
	用户 := models.M用户{F昵称: 用户名, F密码: ofutils.MD5(密码), F邮箱: 邮箱, F创建时间: time.Now()}
	err := DB.Save(&用户)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(用户)
}

// @router /反馈 [get]
func (c *C用户) A反馈() {
	var m models.M反馈
	err := c.ParseForm(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(m)
}

// @router /更新 [get]
func (c *C用户) A更新() {
	if c.UnLogin() {
		return
	}
	key := c.GetString("key")
	value := c.GetString("value")
	用户 := c.GetUser()
	switch key {
	case "F头像", "F邮箱", "F手机", "F昵称":
		err := DB.UpdateField(&用户, key, value)
		if err != nil {
			c.Fail(1, err)
			return
		}
		break
	default:
		c.Fail(2, key+"不允许修改")
		return
	}
	c.Success(用户.G信息())
}

// @router /修改密码 [get]
func (c *C用户) A修改密码() {
	if c.UnLogin() {
		return
	}
	旧密码 := c.GetString("旧密码")
	新密码 := c.GetString("新密码")
	用户 := c.GetUser()
	if ofutils.MD5(旧密码) != 用户.F密码 {
		c.Fail(1, "密码错误")
		return
	}

	用户.F密码 = ofutils.MD5(新密码)
	err := DB.Save(&用户)
	if err != nil {
		c.Fail(2, err)
		return
	}
	c.Success(用户.G信息())
}
