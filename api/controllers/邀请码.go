package controllers

import (
	"markets/api/models"
	"time"

	"github.com/astaxie/beego"
	"github.com/oldfeel/ofutils"
)

type C邀请码 struct {
	BaseController
}

// @router 保存 [get]
func (c *C邀请码) A保存() {
	m := models.M邀请码{F创建时间: time.Now(), F邀请码: ofutils.RandString(8)}
	err := DB.Save(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(m)
}

// @router 分页 [get]
func (c *C邀请码) A分页() {
	页, _ := c.GetInt("页")
	页长, _ := c.GetInt("页长")
	页 = 页 - 1

	var 列表 []models.M邀请码
	query := DB.Select().Limit(页长).Skip(页长 * 页).Reverse()
	err := query.Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	c.Page(列表, &models.M邀请码{}, query)
}
