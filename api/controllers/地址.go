package controllers

import (
	"encoding/json"
	"markets/api/models"
	"time"

	"github.com/astaxie/beego"

	"github.com/asdine/storm/q"
)

type C地址 struct {
	BaseController
}

// @router 保存 [get]
func (c *C地址) A保存() {
	if c.UnLogin() {
		return
	}

	var m models.M地址
	err := c.ParseForm(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}

	m.F用户Id = c.GetUser().Id
	err = DB.Save(&m)
	if err != nil {
		c.Fail(2, err)
		return
	}
	c.Success(m)
}

// @router 列表 [get]
func (c *C地址) A列表() {
	if c.UnLogin() {
		return
	}

	用户 := c.GetUser()
	var 列表 []models.M地址
	err := DB.Select(q.Eq("F用户Id", 用户.Id)).OrderBy("F设置默认的时间").Reverse().Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	c.Success(列表)
}

// @router 设置为默认 [get]
func (c *C地址) A设置为默认() {
	Id, _ := c.GetInt("Id")
	err := DB.UpdateField(&models.M地址{Id: Id}, "F设置默认的时间", time.Now())
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success("设置成功")
}

// @router 默认地址 [get]
func (c *C地址) A默认地址() {
	if c.UnLogin() {
		return
	}

	用户 := c.GetUser()
	var m models.M地址
	err := DB.Select(q.Eq("F用户Id", 用户.Id)).OrderBy("F设置默认的时间").Reverse().First(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}

	c.Success(m)
}

// @router 删除 [get]
func (c *C地址) A删除() {
	Ids := c.GetString("Ids")
	var ids []int
	err := json.Unmarshal([]byte(Ids), &ids)
	if err != nil {
		c.Fail(1, err)
		return
	}

	for _, v := range ids {
		err := DB.Delete("M地址", v)
		if err != nil {
			beego.Info(v, err)
		}
	}
	c.Success("删除成功")
}
