# 市场

便利店/菜市场/超市 买卖/配送 软件.包含 后台管理/客户端手机网页/商户端手机网页

# 目录
 - addVersionCode 增加版本号
 - admin 后台管理的界面,端口 8035
 - api 接口,端口 8034
 - customer-rn 客户端 react native (未开发)
 - business-rn 商家端 react native (未开发)

 # 脚本

  - build.sh 打包项目
  - push.sh 上传到git