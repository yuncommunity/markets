import React, { Component } from 'react'
import { Icon, List, InputItem, Button, NavBar } from 'antd-mobile'
import { createForm } from 'rc-form';
import request from '../../utils/request'
import config from '../../utils/config'
import { routerRedux } from 'dva/router';
import { connect } from 'dva'

class App extends Component {
    constructor(props) {
        super(props)
        let 单位 = {}
        if (props.location.query) {
            单位 = props.location.query.单位
        }
        this.state = {
            error_msg: "",
            单位
        }
    }
    addSubmit = () => {
        const { dispatch, form } = this.props
        const { 单位 } = this.state
        form.validateFields((err, values) => {
            if (!err) {
                request("单位/保存", {
                    ...values,
                    Id: 单位.Id
                }, () => {
                    dispatch(routerRedux.goBack())
                }, (code, msg) => {
                    this.setState({ error_msg: msg })
                })
            }
        })
    }
    render() {
        const { dispatch } = this.props
        const { getFieldProps } = this.props.form;
        const { error_msg, 单位 } = this.state
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.goBack())}
                    rightContent={(
                        <span onClick={this.addSubmit}>确定</span>
                    )}>{单位.Id
                        ? "编辑单位"
                        : "添加单位"}</NavBar>
                <List>
                    {error_msg && (
                        <label
                            style={{
                                color: 'red',
                                padding: 10
                            }}>{error_msg}</label>
                    )}
                    <InputItem
                        {...getFieldProps('F名称', { initialValue: 单位.F名称 }) }
                        clear
                        placeholder="名称">名称</InputItem>
                    <InputItem
                        {...getFieldProps('F简介', { initialValue: 单位.F简介 }) }
                        clear
                        placeholder="简介">简介</InputItem>
                </List>
            </div>
        )
    }
}

export default connect()(createForm()(App))
