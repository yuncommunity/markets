import React, { Component } from 'react'
import {
    NavBar,
    Icon,
    InputItem,
    List,
    ImagePicker,
    WhiteSpace,
    Picker,
    Radio,
    SwipeAction
} from 'antd-mobile'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import { createForm } from 'rc-form';
const ListItem = List.Item
const Brief = ListItem.Brief
const RadioItem = Radio.RadioItem;
import request from '../../utils/request'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            单位列表: []
        }
    }
    componentDidMount = () => {
        this.获取列表()
    }
    获取列表 = () => {
        request("单位/列表", {}, (单位列表) => this.setState({ 单位列表 }), () => this.setState({ 单位列表: [] }))
    }

    render() {
        const { dispatch } = this.props
        const { 单位列表 } = this.state
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.goBack())}
                    rightContent={(
                        <span
                            onClick={() => {
                                dispatch(routerRedux.push("/商家/添加单位"))
                            }}>添加</span>
                    )}>选择单位</NavBar>
                <List>
                    {单位列表.map((单位, index) => {
                        return (
                            <SwipeAction
                                key={index}
                                autoClose
                                right={[
                                    {
                                        text: '取消',
                                        onPress: () => { },
                                        style: {
                                            backgroundColor: '#ddd',
                                            color: 'white'
                                        }
                                    }, {
                                        text: '删除',
                                        onPress: () => {
                                            request("单位/删除", {
                                                Ids: JSON.stringify([单位.Id])
                                            }, () => this.获取列表(), (code, msg) => {
                                                Toast.fail(msg)
                                            })
                                        },
                                        style: {
                                            backgroundColor: '#F4333C',
                                            color: 'white'
                                        }
                                    }, {
                                        text: "编辑",
                                        onPress: () => {
                                            dispatch(routerRedux.push({
                                                pathname: "添加单位", query: {
                                                    单位
                                                }
                                            }))
                                        }
                                    }
                                ]}>
                                <ListItem
                                    onClick={() => {
                                        dispatch({ type: '商家添加产品/save当前产品', F单位: 单位 });
                                        dispatch(routerRedux.goBack());
                                    }}>
                                    {单位.F名称}<Brief>{单位.F简介}</Brief>
                                </ListItem>
                            </SwipeAction>
                        )
                    })}
                </List>
            </div>
        )
    }
}

export default connect()(App)
