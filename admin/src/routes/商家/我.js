import React, { Component } from 'react'
import {
    NavBar,
    Icon,
    List,
    Button,
    WhiteSpace,
    ImagePicker
} from 'antd-mobile'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
const ListItem = List.Item
import config from '../../utils/config'
import request from '../../utils/request'
import Qiniu from '../../utils/qiniu'
import DefaultAvatar from '../../assets/user_normal.png'

@connect(({ global }) => ({ 当前用户: global.当前用户 }))
class App extends Component {
    constructor(props) {
        super(props)
        const { 当前用户 } = props
        const files = []
        if (当前用户.F头像) {
            files.push({ url: 当前用户.F头像, id: 当前用户.Id })
        }
        this.state = {
            files,
            upToken: ""
        }
    }
    componentDidMount = () => {
        request("七牛/token", {}, upToken => {
            this.setState({ upToken })
        })
    }
    上传头像 = (头像) => {
        const { dispatch, 当前用户 } = this.props
        request("用户/更新", {
            key: "F头像",
            value: 头像
        }, (data) => {
            当前用户.F头像 = 头像
            dispatch({ type: 'global/save', 当前用户 })
        })
    }
    render() {
        const { dispatch, 当前用户 } = this.props
        const { files, upToken } = this.state
        return (
            <div>
                <NavBar mode="light">{config.项目名称}</NavBar>
                <List>
                    <ListItem
                        extra={(
                            <Qiniu
                                upToken={upToken}
                                onUpload={this.上传头像}
                                defaultImage={当前用户.F头像
                                    ? 当前用户.F头像
                                    : DefaultAvatar}
                                style={{
                                    width: 48,
                                    height: 48
                                }}></Qiniu>
                        )}>头像</ListItem>
                    <ListItem
                        extra={当前用户.F昵称}
                        arrow="horizontal"
                        onClick={() => {
                            dispatch(routerRedux.push("/商家/用户更新/昵称"))
                        }}>
                        用户名
                    </ListItem>
                    <ListItem
                        extra={当前用户.F邮箱}
                        arrow="horizontal"
                        onClick={() => {
                            dispatch(routerRedux.push("/商家/用户更新/邮箱"))
                        }}>
                        邮箱
                    </ListItem>
                    <ListItem
                        extra={当前用户.F手机}
                        arrow="horizontal"
                        onClick={() => {
                            dispatch(routerRedux.push("/商家/用户更新/手机"))
                        }}>
                        手机
                    </ListItem>
                    <ListItem
                        arrow="horizontal"
                        onClick={() => {
                            dispatch(routerRedux.push("/商家/反馈"))
                        }}>
                        意见反馈
                    </ListItem>
                    <ListItem
                        arrow="horizontal"
                        onClick={() => {
                            dispatch(routerRedux.push("/商家/修改密码"))
                        }}>
                        修改密码
                    </ListItem>
                    <WhiteSpace />
                    <ListItem>
                        <Button
                            type="primary"
                            style={{
                                marginRight: 10
                            }}
                            onClick={() => {
                                dispatch({ type: 'global/退出登录' })
                            }}>
                            退出登录
                        </Button>
                    </ListItem>
                </List>
            </div>
        )
    }
}

export default App
