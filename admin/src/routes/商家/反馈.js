import React, { Component } from 'react'
import {
    NavBar,
    Icon,
    List,
    InputItem,
    Button,
    Modal
} from 'antd-mobile'
import { createForm } from 'rc-form';
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
const ListItem = List.Item
import request from '../../utils/request'

@connect(({ global }) => ({ 当前用户: global.当前用户 }))
@createForm()
class App extends Component {
    提交 = () => {
        const { dispatch, form, 当前用户 } = this.props
        form.validateFields((err, values) => {
            if (!err) {
                request("用户/反馈", {
                    ...values,
                    F用户Id: 当前用户
                }, () => {
                    Modal.alert("反馈成功,谢谢!", null, [
                        {
                            text: "确定",
                            onPress: () => {
                                dispatch(routerRedux.goBack())
                            }
                        }
                    ])
                }, (code, msg) => {
                    Toast.fail(msg)
                })
            }
        })
    }

    render() {
        const { dispatch } = this.props
        const { getFieldProps } = this.props.form;
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.push("/"))}>意见反馈</NavBar>
                <List>
                    <InputItem {...getFieldProps('F内容') } clear placeholder="反馈内容">反馈内容</InputItem>
                    <ListItem>
                        <Button
                            type="primary"
                            style={{
                                marginRight: 10
                            }}
                            onClick={this.提交}>
                            确定
                        </Button>
                    </ListItem>
                </List>
            </div>
        )
    }
}

export default App
