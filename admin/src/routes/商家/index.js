import React, { Component } from 'react'
import { connect } from 'dva'
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { NavBar, Icon, TabBar } from 'antd-mobile';
import 'antd-mobile/dist/antd-mobile.less'
import { getRoutes } from '../../utils/utils'
import Authorized from '../../utils/Authorized'
import NotFound from '../Exception/404'
import config from '../../utils/config'
import request from '../../utils/request'
import Home from './首页'
import Order from './订单'
import Mine from './我'
import ImgHomeNormal from '../../assets/home_normal.png'
import ImgHomeSelect from '../../assets/home_selected.png'
import ImgOrderNormal from '../../assets/order_normal.png'
import ImgOrderSelected from '../../assets/order_selected.png'
import ImgUserNormal from '../../assets/user_normal.png'
import ImgUserSelected from '../../assets/user_selected.png'

const { AuthorizedRoute } = Authorized;

const 标签栏 = {
    "/商家/首页": true,
    "/商家/订单": true,
    "/商家/我": true
}

@connect(({ global }) => ({ 当前用户: global.当前用户 }))
class App extends Component {
    componentDidMount = () => {
        const { dispatch } = this.props
        if (localStorage.Token) {
            dispatch({ type: "global/获取用户信息" })
        }
    }
    是否登录 = () => {
        const { dispatch, 当前用户 } = this.props
        if (!当前用户.Id) {
            dispatch({ type: 'global/退出登录' })
            return false
        }
        return true
    }
    render() {
        const { routerData, match, location, dispatch } = this.props
        if (标签栏[location.pathname]) {
            return (<div
                style={{
                    height: '100%',
                    display: 'flex',
                    flexDirection: "column"
                }}>
                <div style={{ flex: 1 }}>
                    <Switch>
                        <Route path="/商家/首页" component={require("./首页").default} exact />
                        <Route path="/商家/订单" component={require("./订单").default} exact />
                        <Route path="/商家/我" component={require("./我").default} exact />
                    </Switch>
                </div>
                <TabBar unselectedTintColor="#949494" tintColor="#33A3F4" barTintColor="white">
                    <TabBar.Item
                        title="首页"
                        key="首页"
                        icon={(ImgHomeNormal)}
                        selectedIcon={ImgHomeSelect}
                        selected={location.pathname == "/商家/首页"}
                        onPress={() => {
                            dispatch(routerRedux.push("/商家/首页"))
                        }}>
                    </TabBar.Item>
                    <TabBar.Item
                        title="订单"
                        key="订单"
                        icon={ImgOrderNormal}
                        selectedIcon={ImgOrderSelected}
                        selected={location.pathname == "/商家/订单"}
                        onPress={() => {
                            if (this.是否登录()) {
                                dispatch(routerRedux.push("/商家/订单"))
                            }
                        }}>
                    </TabBar.Item>
                    <TabBar.Item
                        title="我"
                        key="我"
                        icon={ImgUserNormal}
                        selectedIcon={ImgUserSelected}
                        selected={location.pathname == "/商家/我"}
                        onPress={() => {
                            if (this.是否登录()) {
                                dispatch(routerRedux.push("/商家/我"))
                            }
                        }}>
                    </TabBar.Item>
                </TabBar>
            </div>)
        } else {
            return (
                <Switch>
                    {
                        getRoutes(match.path, routerData).map(item =>
                            (
                                <AuthorizedRoute
                                    key={item.key}
                                    path={item.path}
                                    component={item.component}
                                    exact={item.exact}
                                    authority={item.authority}
                                    redirectPath="/exception/403"
                                />
                            )
                        )
                    }
                    <Redirect from="/商家" to="/商家/首页"></Redirect>
                    <Route render={NotFound} />
                </Switch>
            )
        }
    }
}

export default App
