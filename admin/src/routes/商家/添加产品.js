import React, { Component } from 'react'
import {
    NavBar,
    Icon,
    InputItem,
    List,
    ImagePicker,
    WhiteSpace,
    Picker,
    Toast,
    ActivityIndicator
} from 'antd-mobile'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
const ListItem = List.Item
const Brief = ListItem.Brief
import request from '../../utils/request'
import utils from '../../utils'

@connect(({ 商家添加产品 }) => ({ 当前产品: 商家添加产品.当前产品 }))
class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            错误信息: "",
            upToken: "",
            animating: false
        }
    }
    componentDidMount = () => {
        request("七牛/token", {}, upToken => this.setState({ upToken }))

        const { dispatch } = this.props
        const Id = utils.网址参数("id")
        if (Id) {
            request("产品/详情", { Id }, 当前产品 => {
                let 图片 = []
                if (当前产品.F图片) {
                    for (let i = 0; i < 当前产品.F图片.length; i++) {
                        图片.push({ url: 当前产品.F图片[i] })
                    }
                }
                当前产品.F图片 = 图片
                dispatch({ type: "商家添加产品/save", 当前产品 })
            })
        }
    }

    上传图片 = () => {
        const _this = this
        const { dispatch, 当前产品: { F图片 } } = this.props
        const { upToken } = this.state
        if (F图片 && F图片.length > 0) {
            for (let i = 0; i < F图片.length; i++) {
                const 图片 = F图片[i]
                // !图片.url.startsWith("http") 说明是新选择的图片, !图片.key 说明是未上传的图片
                if (!图片.key && !图片.url.startsWith("http")) {
                    const url = "http://upload-z2.qiniu.com/putb64/-1";
                    const xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = () => {
                        if (xhr.readyState == 4) {
                            const data = JSON.parse(xhr.responseText)
                            F图片[i].key = data.key
                            dispatch({ type: "客户添加产品/save当前产品", F图片 })
                            _this.上传图片()
                        }
                    }
                    xhr.open("POST", url, true);
                    xhr.setRequestHeader("Content-Type", "application/octet-stream");
                    xhr.setRequestHeader("Authorization", "UpToken " + upToken);
                    xhr.send(图片.url.split(',')[1]);
                    _this.setState({ animating: true })
                    return
                }
            }
            this.确定()
        } else {
            this.确定()
        }
    }
    确定 = () => {
        const { dispatch, 当前产品 } = this.props
        const { F图片 } = 当前产品
        let tmpList = []
        if (F图片.length > 0) {
            for (let i = 0; i < F图片.length; i++) {
                const 图片 = F图片[i]
                if (图片.key) {
                    tmpList.push(F图片[i].key)
                }
                // 图片.url.startsWith("http://ovqiy04p8.bkt.clouddn.com/") 说明是旧图片,只上传名字
                if (图片.url.startsWith("http://ovqiy04p8.bkt.clouddn.com/")) {
                    tmpList.push(图片.url.replace("http://ovqiy04p8.bkt.clouddn.com/", ""))
                }
            }
        }
        当前产品.F图片 = tmpList
        if (!当前产品.F名称) {
            this.setState({ 错误信息: "请输入名称" })
            return
        }
        request("产品/保存", 当前产品, () => {
            dispatch(routerRedux.goBack())
        }, (code, msg) => {
            this.setState({ animating: false, 错误信息: msg })
        })
    }
    render() {
        const { dispatch, 当前产品 } = this.props
        const { 错误信息, animating } = this.state;
        const { F图片 } = 当前产品
        const 单位 = 当前产品.F单位
            ? 当前产品.F单位
            : {}

        const 分类 = 当前产品.F分类
            ? 当前产品.F分类
            : {}
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.push("/商家"))}
                    rightContent={(
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center'
                            }}
                            onClick={this.上传图片}>
                            <ActivityIndicator animating={animating} size="small" />
                            确定
                    </div>
                    )}>{当前产品.Id
                        ? "编辑"
                        : "添加产品"}</NavBar>

                <List>
                    {错误信息 && (
                        <label
                            style={{
                                color: 'red',
                                padding: 10
                            }}>{错误信息}</label>
                    )}
                    <InputItem
                        value={当前产品.F二维码}
                        onChange={(value) => {
                            dispatch({ type: '商家添加产品/save当前产品', F二维码: value })
                        }}
                        clear
                        placeholder="条码"
                        type="number">条码</InputItem>
                    <InputItem
                        value={当前产品.F名称}
                        onChange={(value) => {
                            dispatch({ type: '商家添加产品/save当前产品', F名称: value })
                        }}
                        clear
                        placeholder="名称">名称</InputItem>
                    <InputItem
                        value={当前产品.F简介}
                        onChange={(value) => {
                            dispatch({ type: '商家添加产品/save当前产品', F简介: value })
                        }}
                        clear
                        placeholder="简介">简介</InputItem>
                    <InputItem
                        value={当前产品.F价格}
                        onChange={(value) => {
                            dispatch({ type: '商家添加产品/save当前产品', F价格: value })
                        }}
                        clear
                        placeholder="价格"
                        type="money">价格</InputItem>
                    <ListItem
                        arrow="horizontal"
                        extra={单位.F名称}
                        onClick={() => {
                            dispatch(routerRedux.push("/商家/选择单位"))
                        }}>单位</ListItem>
                    <ListItem
                        arrow="horizontal"
                        extra={分类.F名称}
                        onClick={() => {
                            dispatch(routerRedux.push("/商家/选择分类"))
                        }}>分类</ListItem>
                    <ListItem>
                        图片
                        <ImagePicker
                            files={F图片}
                            onChange={files => dispatch({ type: "商家添加产品/save当前产品", F图片: files })}
                            onImageClick={(index, fs) => console.log(index, fs)}
                            selectable={!F图片 || F图片.length < 9} />
                    </ListItem>
                </List>
            </div>
        )
    }
}

export default App