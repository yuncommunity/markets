import React, { Component } from 'react'
import { NavBar, Icon, List, InputItem, Button } from 'antd-mobile'
import { connect } from 'dva'
import { createForm } from 'rc-form';
import { routerRedux } from 'dva/router'
const ListItem = List.Item
import request from '../../utils/request'
import utils from '../../utils'

@connect()
@createForm()
class App extends Component {
    constructor(props) {
        super(props)
        const query = props.location.query
        this.state = {
            ...query,
            错误信息: ""
        }
    }

    提交 = () => {
        const { dispatch, form } = this.props
        form.validateFields((err, values) => {
            if (!err) {
                if (values.旧密码 == values.新密码) {
                    this.setState({ 错误信息: "新密码不能和旧密码相同" })
                    return
                }
                if (values.新密码 != values.新密码2) {
                    this.setState({ 错误信息: "两次密码不同" })
                    return
                }
                request("用户/修改密码", {
                    ...values
                }, () => {
                    dispatch(routerRedux.goBack())
                }, (code, msg) => {
                    this.setState({ 错误信息: msg })
                })
            } else {
                this.setState({
                    错误信息: utils.getError(err)
                })
            }
        })
    }
    render() {
        const { dispatch } = this.props
        const { 错误信息 } = this.state
        const { getFieldProps } = this.props.form;
        let inputType = "password"
        const rules = [
            {
                required: true
            }
        ]
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.push("/商家"))}>修改密码</NavBar>
                <List>
                    {错误信息 && (
                        <label
                            style={{
                                color: 'red',
                                padding: 10
                            }}>{错误信息}</label>
                    )}
                    <InputItem
                        {...getFieldProps('旧密码', { rules: rules }) }
                        clear
                        placeholder="旧密码"
                        type={inputType}>旧密码</InputItem>
                    <InputItem
                        {...getFieldProps('新密码', { rules: rules }) }
                        clear
                        placeholder="新密码"
                        type={inputType}>新密码</InputItem>
                    <InputItem
                        {...getFieldProps('新密码2', { rules: rules }) }
                        clear
                        placeholder="重复密码"
                        type={inputType}>重复密码</InputItem>
                    <ListItem>
                        <Button
                            type="primary"
                            style={{
                                marginRight: 10
                            }}
                            onClick={this.提交}>
                            确定
                        </Button>
                    </ListItem>
                </List>
            </div>
        )
    }
}

export default App
