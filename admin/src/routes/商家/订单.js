import React, { Component } from 'react'
import { NavBar } from 'antd-mobile'
import config from '../../utils/config'

class App extends Component {
    componentDidMount = () => { }

    render() {
        return (
            <div>
                <NavBar mode="light">{config.项目名称}</NavBar>
            </div>
        )
    }
}

export default App
