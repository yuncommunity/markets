import React, { Component } from 'react'
import {
    NavBar,
    Icon,
    InputItem,
    List,
    ImagePicker,
    WhiteSpace,
    Picker,
    Radio,
    SwipeAction
} from 'antd-mobile'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import { createForm } from 'rc-form';
const ListItem = List.Item
const Brief = ListItem.Brief
const RadioItem = Radio.RadioItem;
import request from '../../utils/request'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list: []
        }
    }
    componentDidMount = () => {
        this.getList()
    }
    getList = () => {
        request("分类/列表", {}, (list) => this.setState({ list }), () => this.setState({ list: [] }))
    }

    render() {
        const { dispatch } = this.props
        const { list } = this.state
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.goBack())}
                    rightContent={(
                        <span
                            onClick={() => {
                                dispatch(routerRedux.push("/商家/添加分类"))
                            }}>添加</span>
                    )}>选择分类</NavBar>
                <List>
                    {list.map((分类, index) => {
                        return (
                            <SwipeAction
                                key={index}
                                autoClose
                                right={[
                                    {
                                        text: '取消',
                                        onPress: () => { },
                                        style: {
                                            backgroundColor: '#ddd',
                                            color: 'white'
                                        }
                                    }, {
                                        text: '删除',
                                        onPress: () => {
                                            request("分类/删除", {
                                                Ids: JSON.stringify([分类.Id])
                                            }, () => this.getList(), (code, msg) => {
                                                Toast.fail(msg)
                                            })
                                        },
                                        style: {
                                            backgroundColor: '#F4333C',
                                            color: 'white'
                                        }
                                    }, {
                                        text: "编辑",
                                        onPress: () => {
                                            dispatch(routerRedux.push({
                                                pathname: "添加分类", query: {
                                                    分类
                                                }
                                            }))
                                        }
                                    }
                                ]}>
                                <ListItem
                                    onClick={() => {
                                        dispatch({ type: '商家添加产品/save当前产品', F分类: 分类 });
                                        dispatch(routerRedux.goBack());
                                    }}>
                                    {分类.F名称}<Brief>{分类.F简介}</Brief>
                                </ListItem>
                            </SwipeAction>
                        )
                    })}
                </List>
            </div>
        )
    }
}

export default connect()(App)
