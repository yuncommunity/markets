import React, { Component } from 'react'
import { Icon, List, InputItem, Button, NavBar } from 'antd-mobile'
import { createForm } from 'rc-form';
import request from '../../utils/request'
import config from '../../utils/config'
import { routerRedux } from 'dva/router';
import { connect } from 'dva'

class App extends Component {
    constructor(props) {
        super(props)
        let 分类 = {}
        if (props.location.query) {
            分类 = props.location.query.分类
        }
        this.state = {
            错误信息: "",
            分类
        }
    }
    addSubmit = () => {
        const { dispatch, form } = this.props
        const { 分类 } = this.state
        form.validateFields((err, values) => {
            if (!err) {
                request("分类/保存", {
                    ...values,
                    Id: 分类.Id
                }, () => {
                    dispatch(routerRedux.goBack())
                }, (code, msg) => {
                    this.setState({ 错误信息: msg })
                })
            }
        })
    }
    render() {
        const { dispatch } = this.props
        const { getFieldProps } = this.props.form;
        const { 错误信息, 分类 } = this.state
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.goBack())}
                    rightContent={(
                        <span onClick={this.addSubmit}>确定</span>
                    )}>{分类.Id
                        ? "编辑分类"
                        : "添加分类"}</NavBar>
                <List>
                    {错误信息 && (
                        <label
                            style={{
                                color: 'red',
                                padding: 10
                            }}>{错误信息}</label>
                    )}
                    <InputItem
                        {...getFieldProps('F名称', { initialValue: 分类.F名称 }) }
                        clear
                        placeholder="名称">名称</InputItem>
                    <InputItem
                        {...getFieldProps('F简介', { initialValue: 分类.F简介 }) }
                        clear
                        placeholder="简介">简介</InputItem>
                </List>
            </div>
        )
    }
}

export default connect()(createForm()(App))
