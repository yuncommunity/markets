import React, { Component } from 'react'
import { NavBar, Icon, List, SwipeAction, ListView, Drawer } from 'antd-mobile'
import { connect } from 'dva'
import { routerRedux, Link } from 'dva/router'
const ListItem = List.Item
import request from '../../utils/request'
import ReactDOM from 'react-dom'
import './首页.less'
import config from '../../utils/config'

class App extends Component {
    constructor(props) {
        super(props)
        const 产品列表 = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2
        });
        this.state = {
            原始数据: [],
            产品列表,
            页: 1,
            打开抽屉: false
        }
    }
    componentDidMount = () => {
        this.getList(1)
    }

    getList = (页) => {
        this.setState({ 页 })
        request("产品/分页", {
            页,
            页长: config.页长,
        }, (新数据) => {
            if (!新数据) {
                新数据 = []
            }
            let { 原始数据 } = this.state
            原始数据 = 原始数据.concat(新数据)
            this.setState({
                产品列表: this.state.产品列表.cloneWithRows(原始数据),
                正在加载: false,
                原始数据
            });
        }, () => {
            this.setState({
                产品列表: this.state.产品列表.cloneWithRows([]),
                正在加载: false,
                原始数据: []
            });
        })
    }
    滑到底部 = (event) => {
        const { 页 } = this.state
        this.getList(页 + 1)
    }

    行 = (产品, sectionID, rowID) => {
        const { dispatch } = this.props
        return (
            <div
                key={rowID}
                style={{
                    padding: 10
                }}
                onClick={() => {
                    dispatch(routerRedux.push("/商家/添加产品?id=" + 产品.Id))
                }}>
                <div
                    style={{
                        display: '-webkit-box',
                        display: 'flex'
                    }}>
                    <img
                        style={{
                            height: 64,
                            width: 64,
                            marginRight: '15px'
                        }}
                        src={产品.F缩略图}
                        alt="" />
                    <div
                        style={{
                            lineHeight: 1,
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'column'
                        }}>
                        <div
                            style={{
                                fontSize: 18
                            }}>{产品.F名称}</div>
                        <div
                            style={{
                                marginBottom: '8px',
                                flex: 1,
                                marginTop: 8
                            }}>{产品.F简介}</div>
                        <div
                            style={{
                                display: 'flex',
                                width: '100%',
                                color: '#888'
                            }}>
                            <span
                                style={{
                                    flex: 1
                                }}>{产品.F分类.F名称}</span>¥{产品.F价格 + 产品.F单位.F名称}
                        </div>
                    </div>
                </div>
            </div>
        );
    };
    切换打开抽屉 = () => {
        this.setState({ 打开抽屉: !this.state.打开抽屉 })
    }
    render() {
        const { dispatch } = this.props
        const { 原始数据, 产品列表, 正在加载, 打开抽屉, } = this.state
        const 分割线 = (sectionID, rowID) => (<div
            key={`${sectionID}-${rowID}`}
            style={{
                backgroundColor: '#F5F5F9',
                height: 8,
                borderTop: '1px solid #ECECED',
                borderBottom: '1px solid #ECECED'
            }} />);
        let 加载状态 = "正在加载..."
        if (!正在加载) {
            if (原始数据.length == 0) {
                加载状态 = "暂无数据"
            } else {
                加载状态 = "加载完成"
            }
        }
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={<Icon type="ellipsis" />}
                    onLeftClick={this.切换打开抽屉}
                    rightContent={(
                        <span
                            onClick={() => {
                                dispatch({ type: '商家添加产品/save', 当前产品: {} });
                                dispatch(routerRedux.push("/商家/添加产品"))
                            }}>添加</span>
                    )}>产品管理</NavBar>
                <Drawer
                    className="my-drawer"
                    style={{ minHeight: document.documentElement.clientHeight - 95 }}
                    open={打开抽屉}
                    onOpenChange={this.切换打开抽屉}
                    sidebar={(<List>
                        <ListItem>
                            <Link to="/">网站</Link>
                        </ListItem>
                        <ListItem>
                            <Link to="/商家">商家</Link>
                        </ListItem>
                        <ListItem>
                            <Link to="/客户">客户</Link>
                        </ListItem>
                    </List>)}
                >
                    <ListView
                        ref={el => this.lv = el}
                        dataSource={this.state.产品列表}
                        renderFooter={() => (
                            <div
                                style={{
                                    padding: 10,
                                    textAlign: 'center'
                                }}>
                                {加载状态}
                            </div>
                        )}
                        renderRow={this.行}
                        renderSeparator={分割线}
                        pageSize={10}
                        style={{
                            height: document.documentElement.clientHeight,
                            overflow: 'auto'
                        }}
                        scrollRenderAheadDistance={500}
                        scrollEventThrottle={200}
                        onEndReached={this.滑到底部}
                        onEndReachedThreshold={10} />
                </Drawer>
            </div >
        )
    }
}

export default connect()(App)
