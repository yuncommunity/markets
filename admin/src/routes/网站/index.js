import React, { Component } from 'react'
import { Link, Switch, Route } from 'dva/router'
import { Card, Button, Menu, Dropdown, Icon, List, Avatar } from 'antd'
import DocumentTitle from 'react-document-title';
import { connect } from 'dva'
import styles from './index.less'
import { getAuthority } from '../../utils/authority'

@connect(({ global }) => ({
    当前用户: global.当前用户,
}))
class App extends Component {
    componentDidMount = () => {
        const { dispatch } = this.props
        if (getAuthority() != 'guest') {
            dispatch({ type: "global/获取用户信息" })
        }
    }

    render() {
        const { dispatch, 当前用户 } = this.props
        return (
            <div style={{ height: '100%', width: '100%' }}>
                <DocumentTitle title="市场" />
                <div style={{
                    paddingLeft: 16,
                    paddingRight: 16,
                    paddingTop: 10,
                    paddingBottom: 10,
                    background: '#002140',
                    boxShadow: '0 1px 4px rgba(0, 21, 41, .08)',
                    display: 'flex',
                    alignItems: 'center',
                    width: '100%'
                }}>
                    <div style={{
                        display: 'flex',
                        alignItems: 'flex-end',
                        flex: 1
                    }}>
                        <div style={{
                            color: 'white',
                            fontSize: '30px',
                            fontWeight: 600
                        }}>市场</div>
                        <div style={{
                            color: 'white',
                            paddingLeft: 10,
                            paddingBottom: 7
                        }}> - 超市/菜市的在线服务/配送平台</div>
                    </div>
                    <Menu mode="horizontal" style={{
                        background: '#002140',
                        borderBottom: "0"
                    }}>
                        <Menu.Item>
                            <Link to="/" style={{ color: 'white' }}>首页</Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to="/关于我" style={{ color: 'white' }}>关于我</Link>
                        </Menu.Item>
                        {当前用户.Id ? (
                            <Menu.SubMenu title={(<div style={{ color: 'white', display: 'flex', alignItems: 'center' }}>
                                {当前用户.F头像 ? (
                                    <Avatar size="small" className={styles.avatar} src={当前用户.F头像} />
                                ) : (<Icon type="user"></Icon>)}
                                <span className={styles.name} style={{ marginLeft: 10 }}>{当前用户.F昵称}</span>
                            </div>)}>
                                <Menu.Item>
                                    <Link to="/后台管理/用户管理">后台管理</Link>
                                </Menu.Item>
                                <Menu.Item>
                                    <Link to="/" onClick={_ => dispatch({ type: "global/退出登录" })}>退出登录</Link>
                                </Menu.Item>
                            </Menu.SubMenu>
                        ) : (
                                <Menu.Item>
                                    <Link style={{ color: 'white' }} to="/用户/登录">登录</Link>
                                </Menu.Item>
                            )}

                    </Menu>
                </div>

                {location.pathname == "/" ? (
                    <div>
                        <Button style={{ margin: 16 }}>
                            <Link to="/商家">商家端</Link>
                        </Button>
                        <Button>
                            <Link to="/客户">客户端</Link>
                        </Button>
                    </div>) : (
                        <Switch>
                            <Route path="/关于我" component={require("./关于我").default}></Route>
                        </Switch>
                    )}

            </div>
        )
    }
}

export default App
