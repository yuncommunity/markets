import React, { Component } from 'react'
import { Row, Col } from 'antd'

class App extends Component {
    render() {
        return (
            <div style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                height: '80%',
                fontSize: '24px',
                fontWeight: 600,
                color: 'black'
            }}>
                QQ: 852566015 <br />
                微信: oldfeel <br />
                邮箱: hyt5926@163.com <br />
            </div>
        )
    }
}

export default App
