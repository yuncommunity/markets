import React, { Component } from 'react'
import {
    NavBar,
    Icon,
    SwipeAction,
    ListView,
    Badge,
    Modal
} from 'antd-mobile'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import request from '../../utils/request'
import config from '../../utils/config'
import ReactDOM from 'react-dom'

const 标题高度 = 45
const 标签栏高度 = 43.5

@connect(({ global, 客户订单 }) => ({ 当前用户: global.当前用户, 客户订单 }))
class App extends Component {
    componentDidMount = () => {
        const 列表高度 = document.documentElement.clientHeight - ReactDOM.findDOMNode(this.lv).parentNode.offsetTop - 标题高度 - 标签栏高度;
        const { dispatch } = this.props
        dispatch({ type: '客户订单/save', 列表高度 })
        this.获取订单列表(1)
    }
    获取订单列表 = (页) => {
        let { dispatch, 客户订单: { 加载状态, 原始数据, dataSource } } = this.props
        if (加载状态 >= 2) {
            return
        }
        request("订单/分页", { 页, 页长: 10 }, (新数据) => {
            if (!新数据) {
                新数据 = []
            }
            原始数据 = 页 == 1 ? 新数据 : 原始数据.concat(新数据)
            加载状态 = 新数据.length < config.页长 ? 2 : 1
            if (!原始数据 || 原始数据.length == 0) {
                加载状态 = 3
            }
            dispatch({
                type: "客户订单/save",
                页,
                加载状态,
                原始数据,
                dataSource: dataSource.cloneWithRows(原始数据.slice()),
            })
        })
    }
    onEndReached = (event) => {
        const { 页 } = this.props.客户订单
        this.获取订单列表(页 + 1)
    }
    行 = (订单, sectionID, rowID) => {
        let 名称 = ""
        let 价格 = 0
        if (订单.F产品列表) {
            for (let i = 0; i < 订单.F产品列表.length; i++) {
                let 产品 = 订单.F产品列表[i]
                名称 += " " + 产品.F名称 + "*" + 产品.F数量
                价格 += 产品.F价格 * 产品.F数量
            }
        }
        return (
            <div
                key={rowID}
                style={{
                    display: '-webkit-box',
                    display: 'flex',
                    padding: 15
                }}
                onClick={_ => {
                    this.props.dispatch(routerRedux.push("订单详情?id=" + 订单.Id))
                }}>
                <div
                    style={{
                        flex: 1,
                        display: 'flex',
                        flexDirection: 'column'
                    }}>
                    <div style={{
                        fontSize: 18
                    }}>{名称}</div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column'
                    }}>
                    <div
                        style={{
                            color: 'red',
                            flex: 1,
                            textAlign: 'right'
                        }}>
                        总价 ¥{价格}
                    </div>
                </div>
            </div>
        );
    };
    renderSeparator = (sectionID, rowID) => (<div
        key={`${sectionID}-${rowID}`}
        style={{
            backgroundColor: '#F5F5F9',
            height: 8,
            borderTop: '1px solid #ECECED',
            borderBottom: '1px solid #ECECED'
        }} />)
    render() {
        const { dispatch, 客户订单 } = this.props
        const { dataSource, 加载状态, 列表高度 } = 客户订单
        let 加载状态文字 = ""
        switch (加载状态) {
            case 0: // 未加载
                加载状态文字 = "正在加载..."
                break;
            case 1: // 正在加载
                加载状态文字 = "正在加载..."
                break;
            case 2: // 加载完成
                加载状态文字 = ""
                break;
            case 3: // 暂无数据
                加载状态文字 = "暂无数据"
                break;
        }
        return (
            <div>
                <ListView
                    ref={el => this.lv = el}
                    dataSource={dataSource}
                    renderFooter={() => (
                        <div
                            style={{
                                padding: 10,
                                textAlign: 'center'
                            }}>
                            {加载状态文字}
                        </div>
                    )}
                    renderRow={this.行}
                    renderSeparator={this.renderSeparator}
                    className="am-list"
                    pageSize={10}
                    style={{
                        height: 列表高度,
                        overflow: 'auto'
                    }}
                    scrollRenderAheadDistance={500}
                    scrollEventThrottle={200}
                    onEndReached={this.onEndReached}
                    onEndReachedThreshold={10}></ListView>
            </div>
        )
    }
}

export default App
