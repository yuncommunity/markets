import React, { Component } from 'react'
import {
    NavBar,
    Icon,
    List,
    SwipeAction,
    ListView,
    Badge,
    Modal
} from 'antd-mobile'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
const ListItem = List.Item
import request from '../../../utils/request'
import config from '../../../utils/config'
import ReactDOM from 'react-dom'
import ImgAdd from '../../../assets/add.png'
import ImgReduce from '../../../assets/reduce.png'
import ImgOrderNormal from '../../../assets/order_normal.png'
import ImgUserNormal from '../../../assets/user_normal.png'
import 底部 from './底部'

const imgStyle = {
    marginLeft: 4,
    marginRight: 4,
    width: 22,
    height: 22
}

const 标题高度 = 45
const 底部高度 = 45
const 标签栏高度 = 43.5

@connect(({ global, 客户首页 }) => ({ 当前用户: global.当前用户, 客户首页 }))
class App extends Component {
    componentDidMount = () => {
        const 列表高度 = document.documentElement.clientHeight - 标题高度 - 底部高度 - 标签栏高度;
        const { dispatch } = this.props
        dispatch({ type: '客户首页/save', 列表高度 })
        this.获取产品列表(1)
    }

    获取产品列表 = (页) => {
        let { dispatch, 客户首页: { dataSource, 加载状态, 原始数据 } } = this.props
        if (加载状态 >= 2) { // 加载完成/暂无数据
            return
        }
        request("产品/分页", {
            页,
            页长: config.页长
        }, (新数据) => {
            if (!新数据) {
                新数据 = []
            }
            原始数据 = 原始数据.concat(新数据)
            加载状态 = 新数据.length < config.页长 ? 2 : 1
            if (!原始数据 || 原始数据.length == 0) {
                加载状态 = 3
            }
            dispatch({
                type: '客户首页/save',
                页,
                加载状态,
                原始数据,
                dataSource: dataSource.cloneWithRows(原始数据.slice())
            })
        }, () => {
            dispatch({
                type: '客户首页/save',
                dataSource: dataSource.cloneWithRows([]),
                原始数据: []
            })
        })
    }
    onEndReached = (event) => {
        const { 页 } = this.props.客户首页
        this.获取产品列表(页 + 1)
    }
    更新列表 = (产品, index) => {
        let { dispatch, 客户首页: { 显示购物车, 原始数据, dataSource } } = this.props;
        原始数据[index] = Object.assign({}, 产品)
        let 已选数量 = 0
        const 已选产品 = []
        for (let i = 0; i < 原始数据.length; i++) {
            if (原始数据[i].F数量 > 0) {
                已选数量 += 原始数据[i].F数量
                已选产品.push(原始数据[i])
            }
        }
        if (已选数量 == 0) {
            显示购物车 = false
        }
        dispatch({
            type: '客户首页/save',
            已选数量,
            已选产品,
            原始数据,
            显示购物车,
            dataSource: dataSource.cloneWithRows(原始数据.slice())
        })
    }
    renderRow = (产品, sectionID, rowID) => {
        return (
            <div
                key={rowID}
                style={{
                    display: '-webkit-box',
                    display: 'flex',
                    padding: 15
                }}>
                <img
                    style={{
                        height: 64,
                        width: 64,
                        marginRight: '15px'
                    }}
                    src={产品.F缩略图}
                    alt="" />
                <div
                    style={{
                        flex: 1,
                        display: 'flex',
                        flexDirection: 'column'
                    }}>
                    <div style={{
                        fontSize: 18
                    }}>{产品.F名称}</div>
                    <div
                        style={{
                            marginBottom: '8px',
                            flex: 1,
                            marginTop: 8,
                            color: '#888'
                        }}>{产品.F简介}</div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column'
                    }}>
                    <div
                        style={{
                            color: 'red',
                            flex: 1,
                            textAlign: 'right'
                        }}>
                        ¥{产品.F价格 + 产品.F单位.F名称}
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'flex-end'
                        }}>
                        {(产品.F数量 > 0) && (<img
                            style={imgStyle}
                            src={ImgReduce}
                            onClick={(e) => {
                                e.stopPropagation();
                                产品.F数量 = 产品.F数量 - 1;
                                产品.位置 = rowID;
                                this.更新列表(产品, rowID);
                            }} />)}
                        {(产品.F数量 > 0) && (
                            <span
                                style={{
                                    lineHeight: imgStyle.height + 'px'
                                }}>{产品.F数量}</span>
                        )}
                        <img
                            style={imgStyle}
                            src={ImgAdd}
                            onClick={(e) => {
                                e.stopPropagation();
                                if (产品.F数量 > 0) {
                                    产品.F数量 = 产品.F数量 + 1;
                                } else {
                                    产品.F数量 = 1
                                }
                                产品.位置 = rowID;
                                this.更新列表(产品, rowID);
                            }} />
                    </div>
                </div>
            </div>
        );
    };
    renderSeparator = (sectionID, rowID) => (<div
        key={`${sectionID}-${rowID}`}
        style={{
            backgroundColor: '#F5F5F9',
            height: 8,
            borderTop: '1px solid #ECECED',
            borderBottom: '1px solid #ECECED'
        }} />)
    render() {
        const { dispatch, 客户首页 } = this.props
        const { 原始数据, dataSource, 加载状态, 列表高度 } = 客户首页
        let 加载状态文字 = ""
        switch (加载状态) {
            case 0: // 未加载
                加载状态文字 = "正在加载..."
                break;
            case 1: // 正在加载
                加载状态文字 = "正在加载..."
                break;
            case 2: // 加载完成
                加载状态文字 = ""
                break;
            case 3: // 暂无数据
                加载状态文字 = "暂无数据"
                break;
        }
        return (
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    height: '100%'
                }}>
                <ListView
                    ref={el => this.lv = el}
                    dataSource={dataSource}
                    renderFooter={() => (
                        <div
                            style={{
                                padding: 10,
                                textAlign: 'center'
                            }}>
                            {加载状态文字}
                        </div>
                    )}
                    renderRow={this.renderRow}
                    renderSeparator={this.renderSeparator}
                    className="am-list"
                    pageSize={10}
                    style={{
                        height: 列表高度,
                        overflow: 'auto'
                    }}
                    scrollRenderAheadDistance={500}
                    scrollEventThrottle={200}
                    onEndReached={this.onEndReached}
                    onEndReachedThreshold={10}></ListView>
                <底部
                    底部高度={底部高度}
                    imgStyle={imgStyle}
                    更新列表={this.更新列表} />
            </div>
        )
    }
}

export default App
