import React, { Component } from 'react'
import { connect } from 'dva'
import { Badge, Modal, List } from 'antd-mobile'
import ImgAdd from '../../../assets/add.png'
import ImgReduce from '../../../assets/reduce.png'
import ShoppingCartNormal from '../../../assets/shopping_cart_normal.png'
import ShoppingCartSelect from '../../../assets/shopping_cart_selected.png'
const ListItem = List.Item
import { routerRedux } from 'dva/router'

@connect(({ 客户首页, global }) => ({
    当前用户: global.当前用户,
    已选数量: 客户首页.已选数量,
    已选产品: 客户首页.已选产品,
    显示购物车: 客户首页.显示购物车
}))
class App extends Component {
    去结算 = (e) => {
        e.stopPropagation()
        const { 当前用户, 已选产品, 已选数量, dispatch } = this.props
        if (!当前用户.Id) { // 未登录
            dispatch(routerRedux.push("/用户/登录"))
            return
        }
        if (已选数量 > 0) {
            dispatch({ type: "客户提交订单/save", 产品列表: 已选产品 })
            dispatch(routerRedux.push('/客户/提交订单'))
        }
    }
    render() {
        const {
            底部高度,
            已选数量,
            已选产品,
            显示购物车,
            imgStyle,
            更新列表,
            dispatch
        } = this.props
        let 总价 = 0
        for (let i = 0; i < 已选产品.length; i++) {
            总价 += 已选产品[i].F价格 * 已选产品[i].F数量
        }
        return (
            <div style={{
                zIndex: 9999,
            }}>
                <div style={{
                    height: 底部高度,
                    background: '#032339',
                    display: 'flex',
                    width: '100%'
                }} onClick={() => {
                    if (已选数量 > 0) {
                        dispatch({ type: "客户首页/save", 显示购物车: !显示购物车 })
                    }
                }}>
                    <div
                        style={{
                            width: 52,
                            height: 52,
                            marginTop: -11,
                            marginLeft: 10,
                            marginRight: 10,
                            marginBottom: 4,
                            background: 已选数量 > 0
                                ? '#33A3F4'
                                : '#ccc',
                            borderRadius: '50%'
                        }}>
                        <Badge
                            text={已选数量}
                            style={{
                                marginTop: 6,
                                marginLeft: 2
                            }}>
                            <img
                                style={{
                                    width: 32,
                                    height: 32,
                                    marginLeft: 10,
                                    marginTop: 10
                                }}
                                src={已选数量 > 0
                                    ? ShoppingCartSelect
                                    : ShoppingCartNormal} />
                        </Badge>
                    </div>
                    <div
                        style={{
                            flex: 1,
                            color: 'white',
                            fontSize: 24,
                            lineHeight: '45px'
                        }}>{总价 > 0 && ("¥" + 总价)}</div>
                    <div
                        style={{
                            width: 100,
                            textAlign: 'center',
                            lineHeight: '45px',
                            color: 'white',
                            background: 已选数量 > 0
                                ? "#33A3F4"
                                : '#333'
                        }}
                        onClick={this.去结算}>去结算</div>
                </div>
                <Modal
                    visible={显示购物车}
                    popup={true}
                    animationType="slide-up"
                    onClose={() => {
                        dispatch({ type: '客户首页/save', 显示购物车: false })
                    }}>
                    <div>
                        <List
                            renderHeader={(
                                <div
                                    style={{
                                        textAlign: 'right'
                                    }}>
                                    <span onClick={(e) => {
                                        e.stopPropagation()
                                        dispatch({ type: "客户首页/清空购物车" })
                                    }}>清空购物车</span>
                                </div>
                            )}>
                            {已选产品.map((产品, index) => {
                                return (
                                    <ListItem key={产品.Id}>
                                        <div
                                            style={{
                                                display: 'flex'
                                            }}>
                                            <div
                                                style={{
                                                    flex: 1
                                                }}>{产品.F名称}</div>
                                            <div
                                                style={{
                                                    marginRight: 10
                                                }}>
                                                ¥{产品.F价格 + 产品.F单位.F名称}
                                            </div>
                                            {(产品.F数量 > 0) && (<img
                                                style={imgStyle}
                                                src={ImgReduce}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                    产品.F数量 = 产品.F数量 - 1;
                                                    更新列表(产品, 产品.位置);
                                                }} />)}
                                            {(产品.F数量 > 0) && (
                                                <span
                                                    style={{
                                                        lineHeight: imgStyle.height + 'px'
                                                    }}>{产品.F数量}</span>
                                            )}
                                            <img
                                                style={imgStyle}
                                                src={ImgAdd}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                    if (产品.F数量 > 0) {
                                                        产品.F数量 = 产品.F数量 + 1;
                                                    } else {
                                                        产品.F数量 = 1
                                                    }
                                                    更新列表(产品, 产品.位置);
                                                }} />
                                        </div>
                                    </ListItem>
                                )
                            })}
                        </List>
                    </div>
                </Modal>
            </div>
        )
    }
}

export default App
