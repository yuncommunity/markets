import React, { Component } from 'react'
import {
  Icon,
  List,
  InputItem,
  Button,
  NavBar,
  Checkbox
} from 'antd-mobile'
import request from '../../utils/request'
import config from '../../utils/config'
import { routerRedux } from 'dva/router';
import { connect } from 'dva'
const ListItem = List.Item

@connect(({ 客户添加地址 }) => ({ 当前地址: 客户添加地址.当前地址 }))
class App extends Component {
  constructor(props) {
    super(props)
  }
  确定 = () => {
    const { dispatch, 当前地址 } = this.props
    request("地址/保存", 当前地址, () => {
      dispatch(routerRedux.goBack())
    }, (code, msg) => {
      dispatch({ type: '客户添加地址/save', 错误信息: msg })
    })
  }
  render() {
    const { dispatch } = this.props
    const { 错误信息, 当前地址 } = this.props
    return (
      <div>
        <NavBar
          mode="light"
          icon={(<Icon type="left" />)}
          onLeftClick={() => dispatch(routerRedux.goBack())}
          rightContent={(
            <span onClick={this.确定}>确定</span>
          )}>{当前地址.Id
            ? "编辑收货地址"
            : "添加收货地址"}</NavBar>
        <List renderHeader="联系人">
          {错误信息 && (
            <label
              style={{
                color: 'red',
                padding: 10
              }}>{错误信息}</label>
          )}
          <InputItem
            value={当前地址.F名称}
            onChange={(F名称) => {
              dispatch({ type: '客户添加地址/save当前地址', F名称 })
            }}
            clear
            placeholder="请输入姓名">姓名</InputItem>
          <ListItem>
            <Checkbox
              style={{
                marginLeft: 90,
                marginRight: 48
              }}
              checked={当前地址.F性别 == "先生"}
              onChange={(e) => {
                const F性别 = e.target.checked
                  ? "先生"
                  : "";
                dispatch({ type: '客户添加地址/save当前地址', F性别 })
              }}>先生</Checkbox>
            <Checkbox
              checked={当前地址.F性别 == "女士"}
              onChange={(e) => {
                const F性别 = e.target.checked
                  ? "女士"
                  : "";
                dispatch({ type: '客户添加地址/save当前地址', F性别 })
              }}>女士</Checkbox>
          </ListItem>
          <InputItem
            value={当前地址.F手机}
            onChange={(F手机) => {
              dispatch({ type: '客户添加地址/save当前地址', F手机 })
            }}
            clear
            type="phone"
            placeholder="请输入收货手机号码,以便配送员联系您">手机</InputItem>
        </List>
        <List renderHeader="收货地址">
          <ListItem
            onClick={() => dispatch(routerRedux.push("/客户/选择位置"))}
            arrow="horizontal"
            extra={当前地址.F地址
              ? 当前地址.F地址
              : "点击选择"}>小区/大厦/学校</ListItem>
          <InputItem
            value={当前地址.F详细地址}
            onChange={(F详细地址) => {
              dispatch({ type: '客户添加地址/save当前地址', F详细地址 })
            }}
            clear
            placeholder="例:1号楼2单元304室"
            labelNumber={8}>楼号-门牌号</InputItem>
        </List>
      </div>
    )
  }
}

export default App
