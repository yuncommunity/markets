import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { NavBar, Icon, Tabs } from 'antd-mobile';
import 'antd-mobile/dist/antd-mobile.less'
import config from '../../utils/config'
import request from '../../utils/request'
import NotFound from '../Exception/404'
import { getRoutes } from '../../utils/utils'
import 首页 from './首页'
import 订单 from './订单'
import 我 from './我'
import Authorized from '../../utils/Authorized'

const { AuthorizedRoute } = Authorized;

const 标签栏 = {
  "/客户/首页": 1,
  "/客户/订单": 2,
  "/客户/我": 3
}

@connect(({ global }) => ({ 当前用户: global.当前用户 }))
class App extends React.Component {
  componentDidMount = () => {
    const { dispatch } = this.props
    if (localStorage.Token) {
      dispatch({ type: "global/获取用户信息" })
    }
  }
  是否登录 = () => {
    const { dispatch, 当前用户 } = this.props
    if (!当前用户.Id) {
      dispatch({ type: 'global/退出登录' })
      return false
    }
    return true
  }
  render() {
    const { routerData, dispatch, match, location } = this.props
    if (标签栏[location.pathname]) {
      return (
        <div
          style={{
            position: 'fixed',
            height: '100%',
            width: '100%',
            top: 0
          }}>
          <NavBar mode="light">{config.项目名称}</NavBar>
          <Tabs initialPage={标签栏[location.pathname] - 1} onChange={(tab, index) => {
            if (index != 1 && !this.是否登录()) {
              return
            }
            dispatch(routerRedux.push(tab.to))
          }} tabs={[
            { title: "首页", to: "/客户/首页" },
            { title: "订单", to: "/客户/订单" },
            { title: "我", to: "/客户/我" }
          ]}>
            <首页 />
            <订单 />
            <我 />
          </Tabs>
        </div>
      )
    } else {
      return (
        <Switch>
          {
            getRoutes(match.path, routerData).map(item =>
              (
                <AuthorizedRoute
                  key={item.key}
                  path={item.path}
                  component={item.component}
                  exact={item.exact}
                  authority={item.authority}
                  redirectPath="/exception/403"
                />
              )
            )
          }
          <Redirect from="/客户" to="/客户/首页"></Redirect>
          <Route render={NotFound} />
        </Switch>
      )
    }
  }
}

export default App
