import React, { Component } from 'react'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import {
    NavBar,
    Icon,
    List,
    InputItem,
    Button,
    WhiteSpace,
    Picker,
    DatePicker,
    Modal,
    Toast
} from 'antd-mobile'
const ListItem = List.Item
const Brief = ListItem.Brief;
import ImgLocation from '../../assets/location.png'
import ImgTime from '../../assets/time.png'
import ImgDefault from '../../assets/default_img.png'
import request from '../../utils/request'
import utils from '../../utils'
import moment from 'moment'

@connect(({ 客户提交订单 }) => ({
    产品列表: 客户提交订单.产品列表,
    地址: 客户提交订单.地址
}))
class App extends Component {
    constructor(props) {
        super(props)
        const 默认时间 = moment().add(40, 'm').toDate()
        this.state = {
            预计送达时间: 默认时间,
            默认时间
        }
    }
    componentDidMount = () => {
        const { 产品列表, dispatch } = this.props
        if (产品列表.length <= 0) {
            Modal.alert("产品列表不能为空,是否返回产品列表?", "", [{
                text: "确定", onPress: _ => {
                    dispatch(routerRedux.push("/客户"))
                }
            }])
        }
        this.获取默认地址()
    }
    获取默认地址 = () => {
        const { dispatch, 地址 } = this.props
        if (!地址.Id) {
            request("地址/默认地址", {}, (地址) => {
                dispatch({ type: '客户提交订单/save', 地址 })
            })
        }
    }
    提交订单 = () => {
        const { 预计送达时间 } = this.state
        const { 产品列表, 地址, dispatch } = this.props
        request("订单/提交", {
            F预计送达时间: moment(预计送达时间).format("YYYY-MM-DD hh:mm"),
            F产品列表: 产品列表,
            F地址: 地址
        }, () => {
            dispatch({ type: "客户首页/清空购物车" })
            Modal.alert("提交成功", "请转账给店长支付宝 hyt5926@163.com,支付成功后在订单中确认支付", [{
                text: "确定", onPress: () => {
                    dispatch(routerRedux.goBack())
                }
            }])
        }, (code, msg) => {
            Modal.alert(msg)
        })
    }
    render() {
        const { 产品列表, dispatch, 地址 } = this.props
        const { 预计送达时间, 默认时间 } = this.state
        let 总价 = 0
        for (let i = 0; i < 产品列表.length; i++) {
            总价 += 产品列表[i].F价格 * 产品列表[i].F数量
        }
        return (
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    height: '100%',
                    background: '#eee'
                }}>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.push("/客户"))}>提交订单</NavBar>
                <List style={{
                    flex: 1
                }}>
                    <ListItem
                        onClick={() => dispatch(routerRedux.push("/客户/选择地址"))}
                        thumb={ImgLocation}
                        arrow="horizontal">{地址.Id
                            ? (地址.F地址 + " " + 地址.F详细地址)
                            : "点击选择"}
                        <Brief>{地址.Id && (地址.F名称 + " " + 地址.F性别 + " " + 地址.F手机)}</Brief>
                    </ListItem>
                    <DatePicker
                        value={预计送达时间}
                        onChange={预计送达时间 => {
                            if (预计送达时间 < 默认时间) {
                                Modal.alert("日期不能小于 " + moment(默认时间).format("MM月DD日 hh:mm"))
                            } else {
                                this.setState({ 预计送达时间 })
                            }
                        }}>
                        <ListItem arrow="horizontal" thumb={ImgTime}>预计送达时间</ListItem>
                    </DatePicker>
                    <div
                        style={{
                            height: 10,
                            background: '#eee',
                            marginBottom: 10
                        }}></div>
                    {产品列表.map((产品, index) => {
                        return (
                            <ListItem
                                thumb={(<img
                                    style={{
                                        width: 64,
                                        height: 64,
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}
                                    src={产品.F缩略图 ? 产品.F缩略图 : ImgDefault} />)}
                                key={index}
                                extra={"¥" + 产品.F价格}>
                                {产品.F名称}<Brief>{"x" + 产品.F数量}</Brief>
                            </ListItem>
                        )
                    })}
                </List>
                <div
                    style={{
                        height: 45,
                        lineHeight: '45px',
                        paddingLeft: 16,
                        background: 'white',
                        display: 'flex'
                    }}>合计
                    <span
                        style={{
                            color: 'red',
                            marginLeft: 10,
                            flex: 1
                        }}>¥{总价}</span>
                    <div
                        onClick={this.提交订单}
                        style={{
                            width: 100,
                            textAlign: 'center',
                            background: '#33A3F4',
                            color: 'white'
                        }}>提交订单</div>
                </div>
            </div>
        )
    }
}

export default App
