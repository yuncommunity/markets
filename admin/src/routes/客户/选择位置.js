import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import {
  Icon,
  List,
  InputItem,
  Button,
  NavBar,
  Checkbox
} from 'antd-mobile'
import { createForm } from 'rc-form';
import request from '../../utils/request'
import config from '../../utils/config'
import { routerRedux } from 'dva/router';
import { connect } from 'dva'
import { Map, Marker } from 'react-amap';
const ListItem = List.Item
const Brief = ListItem.Brief
import ImgLocation from '../../assets/location.png'
import ImgLocationPrimary from '../../assets/location_primary.png'

@connect(({ 客户添加地址 }) => ({ 当前地址: 客户添加地址.当前地址 }))
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      错误信息: "",
      城市: "",
      地址: props.当前地址.F地址
        ? props.当前地址.F地址
        : "",
      热点列表: []
    }
  }
  确定 = () => {
    let { 地址, 热点列表 } = this.state
    if (!地址 && 热点列表.length > 0) {
      地址 = 热点列表[0].name
    }
    const center = this.map.getCenter()
    const { dispatch } = this.props
    dispatch({ type: '客户添加地址/save当前地址', F地址: 地址, F纬度: center.lat, F经度: center.lng })
    dispatch(routerRedux.goBack())
  }
  获取当前城市 = () => {
    const _this = this
    this.map.plugin('AMap.CitySearch', () => {
      const citySearch = new AMap.CitySearch()
      citySearch.getLocalCity((status, { city, info }) => {
        if (status === 'complete' && info === 'OK') {
          _this.setState({ 城市: city })

          AMap.plugin("AMap.Autocomplete", () => {
            const autocomplete = new AMap.Autocomplete({ input: "search_content", city })
            AMap.event.addListener(autocomplete, "select", ({ poi }) => {
              _this.setState({ 地址: poi.name })
              _this.map.setCenter(poi.location)
              _this.placeSearch.searchNearBy(poi.name, poi.location, 1000)
            })
          })

          AMap.plugin('AMap.PlaceSearch', () => {
            _this.placeSearch = new AMap.PlaceSearch({ city, type: "商务住宅" });
            _this.获取热点列表()
            AMap.event.addListener(_this.placeSearch, "selectChanged", ({ selected }) => {
              _this.map.setCenter(selected.data.location)
            })
          });
        }
      })
    })
    const { 当前地址 } = this.props
    if (当前地址.F纬度) {
      const lngLat = new AMap.LngLat(当前地址.F纬度, 当前地址.F经度)
      this.map.setCenter(lngLat)
    }
  }
  获取热点列表 = () => {
    this.placeSearch.searchNearBy('', this.map.getCenter(), 500, (status, { poiList, info }) => {
      if (status === "complete" && info === "OK") {
        this.setState({ 热点列表: poiList.pois, 地址: poiList.pois[0].name })
      }
    })
  }
  render() {
    const _this = this
    const { dispatch } = this.props
    const { 错误信息, 城市, 地址, 热点列表 } = this.state
    return (
      <div>
        <NavBar
          mode="light"
          icon={(<Icon type="left" />)}
          onLeftClick={() => dispatch(routerRedux.goBack())}
          rightContent={(
            <span onClick={this.确定}>确定</span>
          )}>选择小区/大厦/学校</NavBar>
        <div
          style={{
            height: 45,
            display: 'flex',
            alignItems: 'center',
            background: 'white',
            paddingLeft: 10,
            paddingRight: 10
          }}>
          <img
            style={{
              width: 22,
              height: 22
            }}
            src={ImgLocation} /> {城市}
          <InputItem
            id="search_content"
            clear
            value={地址}
            onChange={(地址) => {
              this.setState({ 地址 })
            }}
            placeholder={热点列表.length > 0
              ? 热点列表[0].name
              : "查找小区/大厦/学校等"}></InputItem>
        </div>
        <div style={{
          height: 400
        }}>
          <div
            ref={el => this.lv = el}
            style={{
              width: '100%',
              height: 400
            }}>
            <Map
              amapkey={"4156ac684dc26ceb599f720a555338e5"}
              events={{
                created(map) {
                  _this.map = map;
                  map.setZoom(15);
                  _this.获取当前城市()
                },
                moveend() {
                  _this.获取热点列表()
                }
              }}
              plugins={['ToolBar']} />
          </div>
          <img
            style={{
              position: 'absolute',
              width: 32,
              height: 32,
              top: 261,
              left: 0,
              right: 0,
              margin: 'auto auto'
            }}
            src={ImgLocationPrimary}></img>
        </div>
        <List>
          {热点列表.map((热点, index) => {
            return (
              <ListItem
                key={index}
                onClick={() => {
                  this.map.setCenter(热点.location)
                }}>{热点.name}
                <Brief>{热点.address}</Brief>
              </ListItem>
            )
          })}
        </List>
      </div>
    )
  }
}

export default App
