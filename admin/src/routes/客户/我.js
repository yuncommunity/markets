import React, { Component } from 'react'
import { NavBar, Icon, List, Button, WhiteSpace } from 'antd-mobile'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
const ListItem = List.Item
import config from '../../utils/config'

@connect(({ global }) => ({ 当前用户: global.当前用户 }))
class App extends Component {
    componentDidMount = () => {
    }

    render() {
        const { dispatch, 当前用户 } = this.props
        return (
            <div>
                <List>
                    <ListItem extra={(<img src={当前用户.F头像} />)}>
                        头像
                    </ListItem>
                    <ListItem extra={当前用户.F昵称}>
                        用户名
                    </ListItem>
                    <ListItem extra={当前用户.F邮箱}>
                        邮箱
                    </ListItem>
                    <ListItem extra={当前用户.F手机}>
                        手机
                    </ListItem>
                    <ListItem
                        arrow="horizontal"
                        onClick={() => {
                            dispatch(routerRedux.push("/客户/反馈"))
                        }}>
                        意见反馈
                    </ListItem>
                    <WhiteSpace />
                    <ListItem>
                        <Button
                            type="primary"
                            style={{
                                marginRight: 10
                            }}
                            onClick={() => {
                                dispatch({ type: 'global/退出登录' })
                            }}>
                            退出
                        </Button>
                    </ListItem>
                </List>
            </div>
        )
    }
}

export default App
