import React, { Component } from 'react'
import {
  NavBar,
  Icon,
  List,
  InputItem,
  Button,
  SwipeAction,
  Modal
} from 'antd-mobile'
import { createForm } from 'rc-form';
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
const ListItem = List.Item
const Brief = ListItem.Brief
import request from '../../utils/request'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      地址列表: []
    }
  }

  componentDidMount = () => {
    this.获取地址列表()
  }

  获取地址列表 = () => {
    request("地址/列表", {}, (地址列表) => this.setState({
      地址列表: 地址列表 ? 地址列表 : []
    }))
  }

  render() {
    const { dispatch } = this.props
    const { 地址列表 } = this.state
    return (
      <div>
        <NavBar
          mode="light"
          icon={(<Icon type="left" />)}
          onLeftClick={() => dispatch(routerRedux.goBack())}
          rightContent={(
            <span
              onClick={() => {
                dispatch({ type: '客户添加地址/save', 当前地址: {} });
                dispatch(routerRedux.push("/客户/添加地址"));
              }}>添加</span>
          )}>选择收货地址</NavBar>
        <List>
          {地址列表.map((地址, index) => {
            return (
              <SwipeAction
                key={index}
                autoClose
                right={[
                  {
                    text: '取消',
                    onPress: () => { },
                    style: {
                      backgroundColor: '#ddd',
                      color: 'white'
                    }
                  }, {
                    text: '删除',
                    onPress: () => {
                      Modal.alert('删除', "确定要删除吗?", [{
                        text: "取消"
                      }, {
                        text: "确定",
                        onPress: () => {
                          request("地址/删除", {
                            Ids: JSON.stringify([地址.Id])
                          }, () => this.获取地址列表(), (code, msg) => {
                            Toast.fail(msg)
                          })
                        }
                      }])
                    },
                    style: {
                      backgroundColor: '#F4333C',
                      color: 'white'
                    }
                  }, {
                    text: "编辑",
                    onPress: () => {
                      dispatch({ type: '客户添加地址/save', 当前地址: 地址 });
                      dispatch(routerRedux.push("/客户/添加地址"))
                    }
                  }
                ]}>
                <ListItem
                  onClick={() => {
                    dispatch({ type: '客户提交订单/save', 地址 });
                    dispatch(routerRedux.goBack())
                  }}
                  extra={index == 0
                    ? (
                      <span style={{
                        color: 'red'
                      }}>默认地址</span>
                    )
                    : (
                      <span
                        onClick={(e) => {
                          e.stopPropagation();
                          request("地址/设置为默认", {
                            Id: 地址.Id
                          }, () => {
                            this.获取地址列表()
                          })
                        }}
                        style={{
                          color: '#108ee9'
                        }}>设为默认</span>
                    )}>{地址.F地址 + " " + 地址.F详细地址}
                  <Brief>{地址.F名称 + " " + 地址.F性别 + " " + 地址.F手机}</Brief>
                </ListItem>
              </SwipeAction>
            )
          })}
        </List>
      </div>
    )
  }
}

export default connect()(App)
