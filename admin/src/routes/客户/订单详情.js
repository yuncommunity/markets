import React, { Component } from 'react'
import {
    Icon,
    List,
    InputItem,
    Button,
    NavBar,
    Checkbox,
    Modal,
} from 'antd-mobile'
import request from '../../utils/request'
import utils from '../../utils'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
const ListItem = List.Item
const Brief = ListItem.Brief;
import ImgLocation from '../../assets/location.png'
import ImgDefault from '../../assets/default_img.png'
import ImgTime from '../../assets/time.png'

@connect()
class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            当前订单: {
                F地址: {},
                F产品列表: [],
                预计送达时间: ""
            }
        }
    }
    componentDidMount = () => {
        request("订单/详情", { Id: utils.网址参数("id") },
            当前订单 => this.setState({ 当前订单 }),
            (返回码, 错误信息) => Modal.alert("订单不存在", "", [{ text: "确定", onPress: _ => this.props.dispatch(routerRedux.goBack()) }]))
    }

    render() {
        const { dispatch } = this.props
        const { 当前订单 } = this.state
        const 地址 = 当前订单.F地址
        const 产品列表 = 当前订单.F产品列表
        const 预计送达时间 = 当前订单.F预计送达时间
        return (
            <div>
                <NavBar
                    mode="light"
                    icon={(<Icon type="left" />)}
                    onLeftClick={() => dispatch(routerRedux.goBack())}
                >订单详情</NavBar>

                <List style={{
                    flex: 1
                }}>
                    <ListItem thumb={ImgLocation}>
                        {地址.Id
                            ? (地址.F地址 + " " + 地址.F详细地址)
                            : "点击选择"}
                        <Brief>{地址.Id && (地址.F名称 + " " + 地址.F性别 + " " + 地址.F手机)}</Brief>
                    </ListItem>
                    <ListItem thumb={ImgTime}>
                        预计送达时间: {预计送达时间}
                    </ListItem>
                    <div
                        style={{
                            height: 10,
                            background: '#eee',
                            marginBottom: 10
                        }}></div>
                    {产品列表.map((产品, index) => {
                        return (
                            <ListItem
                                thumb={(<img
                                    style={{
                                        width: 64,
                                        height: 64,
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}
                                    src={产品.F缩略图 ? 产品.F缩略图 : ImgDefault} />)}
                                key={index}
                                extra={"¥" + 产品.F价格}>
                                {产品.F名称}<Brief>{"x" + 产品.F数量}</Brief>
                            </ListItem>
                        )
                    })}
                </List>
            </div>
        )
    }
}

export default App
