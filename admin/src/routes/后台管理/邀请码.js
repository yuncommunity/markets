import React, { Component } from 'react'
import { Table, Button, Input, Popconfirm, message } from 'antd'
import request from '../../utils/request'
import utils from '../../utils'
import config from '../../utils/config'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            页: 1,
            页长: config.页长,
            总数: 0,
            邀请码: [],
            正在加载: false,
        }
    }
    componentDidMount = () => {
        this.获取邀请码()
    }
    获取邀请码 = () => {
        this.setState({ 正在加载: true })

        const { 页, 页长 } = this.state
        request("邀请码/分页", {
            页, 页长
        }, (邀请码, 总数) => {
            this.setState({ 邀请码, 总数, 正在加载: false })
        })
    }

    render() {
        const { 页, 页长, 总数, 邀请码, 正在加载 } = this.state
        const 列 = [{
            title: "ID",
            dataIndex: "Id",
        }, {
            title: "邀请码",
            dataIndex: "F邀请码",
        }, {
            title: "使用者",
            dataIndex: "F使用者.F昵称",
        }, {
            title: "使用时间",
            dataIndex: "F使用时间",
            render: (text, record) => {
                return utils.格式化时间(text)
            }
        }, {
            title: "创建时间",
            dataIndex: "F创建时间",
            render: (text, record) => {
                return utils.格式化时间(text)
            }
        }]
        return (
            <div>
                <Button type="primary" style={{
                    marginBottom: 16
                }} onClick={_ => {
                    request("邀请码/保存", {}, () => {
                        this.获取邀请码()
                    })
                }}>添加</Button>
                <Table rowKey="Id"
                    loading={正在加载}
                    bordered={true}
                    pagination={{
                        showSizeChanger: true,
                        current: 页,
                        pageSize: 页长,
                        total: 总数
                    }}
                    onChange={(分页, 筛选, 排序) => {
                        this.setState({ 页: 分页.current, 页长: 分页.pageSize }, () => {
                            this.获取邀请码()
                        })
                    }
                    }
                    columns={列}
                    dataSource={邀请码 ? 邀请码 : []} />
            </div>
        )
    }
}

export default App
