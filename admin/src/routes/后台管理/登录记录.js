import React, { Component } from 'react'
import { Table, Button, Input, Popconfirm, message } from 'antd'
import request from '../../utils/request'
import utils from '../../utils'
import config from '../../utils/config'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            页: 1,
            页长: config.页长,
            总数: 0,
            登录记录: [],
            正在加载: false,
        }
    }
    componentDidMount = () => {
        this.获取登录记录()
    }
    获取登录记录 = () => {
        this.setState({ 正在加载: true })

        const { 页, 页长 } = this.state
        request("登录记录/分页", {
            页, 页长
        }, (登录记录, 总数) => {
            this.setState({ 登录记录, 总数, 正在加载: false })
        })
    }

    render() {
        const { 页, 页长, 总数, 登录记录, 正在加载 } = this.state
        const 列 = [{
            title: "ID",
            dataIndex: "Id",
        }, {
            title: "用户名",
            dataIndex: "F用户.F昵称",
        }, {
            title: "Ip",
            dataIndex: "Ip",
        }, {
            title: "地址",
            dataIndex: "F地址",
        }, {
            title: "创建时间",
            dataIndex: "F创建时间",
            render: (text, record) => {
                return utils.格式化时间(text)
            }
        }]
        return (
            <div>
                <Table rowKey="Id"
                    loading={正在加载}
                    bordered={true}
                    pagination={{
                        showSizeChanger: true,
                        current: 页,
                        pageSize: 页长,
                        total: 总数
                    }}
                    onChange={(分页, 筛选, 排序) => {
                        this.setState({ 页: 分页.current, 页长: 分页.pageSize }, () => {
                            this.获取登录记录()
                        })
                    }
                    }
                    columns={列}
                    dataSource={登录记录 ? 登录记录 : []} />
            </div>
        )
    }
}

export default App
