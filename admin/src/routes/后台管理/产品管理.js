import React, { Component } from 'react'
import { Table, Button, Input, Popconfirm, message } from 'antd'
import request from '../../utils/request'
import utils from '../../utils'
import config from '../../utils/config'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            页: 1,
            页长: config.页长,
            总数: 0,
            产品列表: [],
            已选Id: [],
            正在加载: false,
            搜索: ""
        }
    }
    componentDidMount = () => {
        this.获取产品列表()
    }
    获取产品列表 = () => {
        this.setState({ 正在加载: true })

        const { 页, 页长, 搜索 } = this.state
        request("产品/分页", {
            页, 页长, 搜索
        }, (产品列表, 总数) => {
            this.setState({ 产品列表, 总数, 正在加载: false })
        })
    }

    render() {
        const { 页, 页长, 总数, 产品列表, 正在加载, 已选Id } = this.state
        const 列 = [{
            title: "ID",
            dataIndex: "Id",
        }, {
            title: "缩略图",
            dataIndex: "F缩略图",
        }, {
            title: "名称",
            dataIndex: "F名称",
        }, {
            title: "简介",
            dataIndex: "F简介",
        }, {
            title: "分类",
            dataIndex: "F分类.F名称",
        }, {
            title: "价格",
            dataIndex: "F价格",
        }, {
            title: "单位",
            dataIndex: "F单位.F名称",
        }, {
            title: "创建时间",
            dataIndex: "F创建时间",
            render: (text, record) => {
                return utils.格式化时间(text)
            }
        }]
        return (
            <div>
                <div style={{ marginBottom: 16, display: 'flex', justifyContent: 'space-between' }}>
                    <Input.Search style={{ width: 256 }} enterButton placeholder="输入产品名称" onSearch={搜索 => {
                        this.setState({ 搜索, 页: 1 }, () => {
                            this.获取产品列表()
                        })
                    }} />

                    {已选Id.length > 0 && (<Popconfirm title="确定要删除吗?" onConfirm={() => {
                        request("产品/删除", {
                            Ids: JSON.stringify(已选Id)
                        }, () => {
                            if (页 > 1 && 已选Id.length == 产品列表.length) {
                                this.setState({ 页: 页 - 1 }, _ => this.获取产品列表())
                            } else {
                                this.获取产品列表()
                            }
                        }, (code, msg) => {
                            message(msg)
                        })
                    }}>
                        <Button type="danger">删除</Button>
                    </Popconfirm>)}
                </div>
                <Table rowKey="Id" loading={正在加载} bordered={true} rowSelection={{
                    onChange: (已选Id) => {
                        this.setState({ 已选Id })
                    },
                }} pagination={{
                    showSizeChanger: true,
                    current: 页,
                    pageSize: 页长,
                    total: 总数
                }} onChange={(分页, 筛选, 排序) => {
                    this.setState({ 页: 分页.current, 页长: 分页.pageSize }, () => {
                        this.获取产品列表()
                    })
                }
                } columns={列} dataSource={产品列表 ? 产品列表 : []} />
            </div>
        )
    }
}

export default App
