import { ListView } from 'antd-mobile'
export default {
  namespace: "客户首页",
  state: {
    已选数量: 0,
    已选产品: [],
    显示购物车: false,
    dataSource: new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2
    }),
    原始数据: [],
    列表高度: 0,
    加载状态: 0 // 0:未加载,1:正在加载,2:加载完成,3:暂无数据
  },
  effects: {
    *fetch({ payload }, { call, put }) {
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action
      };
    },
    清空购物车(state) {
      const { 原始数据, dataSource } = state
      for (let i = 0; i < 原始数据.length; i++) {
        原始数据[i] = {
          ...原始数据[i],
          F数量: 0
        }
      }
      return {
        ...state,
        已选数量: 0,
        已选产品: [],
        显示购物车: false,
        原始数据,
        dataSource: dataSource.cloneWithRows(原始数据.slice())
      }
    }
  },
};

