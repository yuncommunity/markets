
import request from '../utils/request'
import { routerRedux } from 'dva/router';
import { setAuthority } from '../utils/authority'
export default {
  namespace: 'global',
  state: {
    collapsed: false,
    当前用户: {}
  },
  effects: {
    *获取用户信息({ }, { put }) {
      const 请求结果 = yield request("用户/获取用户信息")
      if (请求结果.返回码 == 0) {
        yield put({ type: 'save', 当前用户: 请求结果.数据 })
      }
    },
    *退出登录({ }, { put }) {
      localStorage.Token = ""
      setAuthority("guest")
      location.href = location.origin + "/用户/登录"
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action
      }
    },
    save当前用户(state, action) {
      const { 当前用户 } = state
      return {
        ...state,
        当前用户: {
          ...当前用户,
          ...action
        }
      }
    },
    changeLayoutCollapsed(state, { payload }) {
      return {
        ...state,
        collapsed: payload,
      };
    },
  },

  subscriptions: {
    setup({ history }) {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      return history.listen(({ pathname, search }) => {
        if (typeof window.ga !== 'undefined') {
          window.ga('send', 'pageview', pathname + search);
        }
      });
    },
  },
};
