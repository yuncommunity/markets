import utils from '../utils'
export default {
    namespace: '商家添加产品',
    state: {
        当前产品: {
            F图片: []
        }
    },
    reducers: {
        save(state, action) {
            return { ...state, ...action }
        },
        save当前产品(state, action) {
            const { 当前产品 } = state
            return {
                ...state,
                当前产品: {
                    ...当前产品,
                    ...action
                }
            }
        },
    },
    effects: {},
    subscriptions: {}
};
