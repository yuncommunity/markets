import { ListView } from 'antd-mobile'
export default {
    namespace: '客户订单',
    state: {
        dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2
        }),
        原始数据: [],
        列表高度: 0,
        加载状态: 0 // 0:未加载,1:正在加载,2:加载完成,3:暂无数据
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action
            }
        }
    },
    effects: {
    },
    subscriptions: {},
};
