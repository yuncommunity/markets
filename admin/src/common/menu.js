import { isUrl } from '../utils/utils';

const menuData = [{
  name: "用户管理",
  path: "后台管理/用户管理"
}, {
  name: "产品管理",
  path: "后台管理/产品管理"
}, {
  name: "登录记录",
  path: "后台管理/登录记录"
}, {
  name: "邀请码",
  path: "后台管理/邀请码"
}];

function formatter(data, parentPath = '', parentAuthority) {
  return data.map((item) => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}

export const getMenuData = () => formatter(menuData);
