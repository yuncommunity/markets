import React from 'react';
import { routerRedux, Switch, Route } from 'dva/router';
import { LocaleProvider, Spin } from 'antd';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import dynamic from 'dva/dynamic';
import { getRouterData } from './common/router';
import Authorized from './utils/Authorized';
import styles from './index.less';

const { ConnectedRouter } = routerRedux;
const { AuthorizedRoute } = Authorized;
dynamic.setDefaultLoadingComponent(() => {
  return <Spin size="large" className={styles.globalSpin} />;
});

function RouterConfig({ history, app }) {
  const routerData = getRouterData(app);
  const UserLayout = routerData['/用户'].component;
  const BasicLayout = routerData['/后台管理'].component;
  const 网站 = routerData['/网站'].component;
  const 商家 = routerData['/商家'].component;
  const 客户 = routerData['/客户'].component;
  return (
    <LocaleProvider locale={zhCN}>
      <ConnectedRouter history={history}>
        <Switch>
          <AuthorizedRoute
            path="/用户"
            render={props => <UserLayout {...props} />}
            authority="guest"
            redirectPath="/"
          />
          <AuthorizedRoute
            path="/后台管理"
            render={props => <BasicLayout {...props} />}
            authority={['admin', 'user']}
            redirectPath="/用户/登录"
          />
          <AuthorizedRoute
            path="/商家"
            render={props => <商家 {...props} />}
            authority={['admin']}
            redirectPath="/用户/登录"
          />
          <AuthorizedRoute
            path="/客户"
            render={props => <客户 {...props} />}
            authority={['admin']}
            redirectPath="/用户/登录"
          />
          <Route
            path="/"
            render={props => <网站 {...props} />}
          />
        </Switch>
      </ConnectedRouter>
    </LocaleProvider>
  );
}

export default RouterConfig;
