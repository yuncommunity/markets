module.exports = {
    格式化时间(时间) {
        if (!时间) {
            return ""
        }
        if (时间.startsWith("000")) {
            return "-"
        }
        return 时间.substr(0, 19).replace("T", " ")
    },
    获取文件名(url) {
        return url.substring(url.lastIndexOf("/") + 1)
    },
    网址参数(name) {
        var matches = location.href.match(new RegExp(name + '=([^&]*)'))
        return matches
            ? matches[1]
            : null;
    }
}