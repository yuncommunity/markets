import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Modal, Slider, Button } from 'antd-mobile'
import AvatarEditor from 'react-avatar-editor'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imageUrl: "",
            imageResult: "",
            visible: false,
            scale: 20
        }
    }
    onClick = () => {
        const fileInput = ReactDOM.findDOMNode(this.refs.fileInput);
        fileInput.value = null;
        fileInput.click();
    }
    onChange = (e) => {
        const _this = this
        let files
        if (e.dataTransfer) {
            files = e.dataTransfer.files
        } else if (e.target) {
            files = e.target.files
        }
        if (files.length > 0) {
            const reader = new FileReader()
            reader.addEventListener('load', () => {
                _this.setState({ imageUrl: reader.result, visible: true })
            })
            reader.readAsDataURL(files[0])
        }
    }
    onCrop = () => {
        let img64 = this.refs.image_editor.getImage().toDataURL()
        this.setState({ visible: false, imageResult: img64 })

        const { upToken, onUpload } = this.props
        let pic = img64.split(",")[1]
        let url = "http://upload-z2.qiniu.com/putb64/-1";
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                let data = JSON.parse(xhr.responseText)
                if (onUpload) {
                    onUpload(data.key)
                }
            }
        }
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/octet-stream");
        xhr.setRequestHeader("Authorization", "UpToken " + upToken);
        xhr.send(pic);
    }
    render() {
        const { accept, style, defaultImage } = this.props
        const { imageUrl, visible, scale, imageResult } = this.state
        return (
            <div>
                <div onClick={this.onClick}>
                    <input
                        style={{
                            display: 'none'
                        }}
                        accept={accept}
                        ref="fileInput"
                        type='file'
                        onChange={this.onChange} />
                    <img
                        style={style}
                        src={imageResult
                            ? imageResult
                            : defaultImage} />
                </div>
                <Modal
                    visible={visible}
                    title="裁剪"
                    closable
                    transparent={true}
                    maskClosable={false}
                    onClose={() => {
                        this.setState({ visible: false })
                    }}>
                    <div>
                        <AvatarEditor
                            ref="image_editor"
                            image={imageUrl}
                            width={200}
                            height={200}
                            border={20}
                            scale={scale / 100 + 1} />
                        <div
                            style={{
                                paddingTop: 10,
                                paddingBottom: 20
                            }}>
                            <Slider
                                onChange={(value) => {
                                    this.setState({ scale: value })
                                }}
                                defaultValue={20} />
                        </div>

                        <Button type="primary" size="small" onClick={this.onCrop}>确定</Button>
                    </div>
                </Modal>
            </div>
        )
    }
}

export default App
