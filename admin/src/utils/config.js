let config = {
    项目名称: "超市",
    接口网址: "http://localhost:8034/接口/",
    验证码网址: "http://localhost:8034/验证码图片/",
    默认地址: "/后台管理/用户管理",
    版权所有: "云社区",
    页长: 10
}

if (process.env.NODE_ENV != "development") {
    Object.assign(config, {
        接口网址: document.location.origin + "/接口/",
        验证码网址: document.location.origin + "/验证码图片/",
    })
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

module.exports = config