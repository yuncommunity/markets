package main

import (
	"fmt"

	"github.com/astaxie/beego/config"
	"github.com/oldfeel/ofutils"
)

func main() {
	filename := "../api/conf/app.conf"
	conf, err := config.NewConfig("ini", filename)
	if err != nil {
		fmt.Println("conf err", err)
		return
	}
	versionCode, _ := conf.Int("version_code")
	conf.Set("version_code", ofutils.ToString(versionCode+1))
	conf.SaveConfigFile(filename)
}
